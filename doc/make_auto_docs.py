###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
This script is used to automatically create the documentation pages for the scripts in
DaVinciExamples and DaVinciTutorials and show them in the section Examples and Tutorials
of the DaVinci documentation site.

The script so far creates the documentation pages based on information added on top of
the original python files.
The information is formatted in the form of a comment

"""
<minimal description of the python script>

rst_title: <documentation page title>
rst_description: <documentation page description>
rst_running: <script to run the example/tutorial>
rst_yaml: <address of the yaml file relative to the 'doc' folder in DaVinci>
"""

Further sections can be added if needed.
The information above is decoded in the function GetPageInfo of make_auto_docs.py and used in the functions WritePage to create the page.
The index for DaVinciExamples or DaVinciTutorials and their respective sub-folders is written by the function WriteIndex that is called recursively when a sub-folder is found.
'''
import os, warnings


def GetPageInfo(script):
    """A function to extract the information to write the documentation page from the script

    Args:
        script (str): the file name of the script

    Raises:
        Warning: invalid documentation keys

    Returns:
        dict: a dictionary with information to write the documentation page
    """
    info = {"title": None, "description": None, "running": None, "yaml": None}
    f = open(script)
    active_comment = False
    active_key = None
    line_start_code = 0
    for l in f:
        line_start_code += 1
        if active_comment and ('"""' in l or "'''" in l):
            break  # avoid looping on the whole file, stop search after the first comment
        if '"""' in l or "'''" in l:
            active_comment = not active_comment
        if "rst_" not in l and active_key is None:  # or l == '\n':
            continue  # ignore lines without sphinx info
        if "rst_" in l:
            active_key = l[l.rfind("rst_") + 4 : l.find(":")]  # extract the key
            if active_key not in info.keys():
                warnings.warn(f"The key {active_key} in {script} is not valid!")
            info[active_key] = l[
                l.find(":") + 1 :
            ].lstrip()  # l.split(':')[1].lstrip()  # save information
        else:
            info[active_key] += l
    for k in info.keys():
        if k != "description" and info[k] is not None:
            info[k] = info[k].replace("\n", "")
    for k in ["yaml"]:
        if info[k] is not None:
            yaml_files = info["yaml"].split("+")
            for yf in yaml_files:
                if not os.path.exists(yf.strip()):
                    warnings.warn(f"The {k} path is not valid: {yf}")
    info["line_start_code"] = line_start_code
    return info


def WritePage(script, info, directory="."):
    """A function to write the documentation page

    Args:
        script (str): the file name of the script
        info (dict): a dictionary with information to write the documentation page
    """
    # Title
    out = info["title"] + "\n"
    out += "=" * len(info["title"]) + "\n\n"
    # Description
    if info["description"] is not None:
        out += info["description"] + "\n\n"
    # Include script
    out += f"..  literalinclude:: /{script}\n"
    out += "    :language: python\n"
    if info["line_start_code"] != 0:
        out += f'    :lines: {info["line_start_code"]+1}-\n'
    out += "    :class: toggle\n\n"
    # How-to run
    if info["running"] is not None:
        out += "To run the example:\n\n"
        out += ".. code-block:: sh\n\n"
        out += f'    {info["running"]}\n\n'
    # Yaml Options
    if info["yaml"] is not None:
        out += "For reference, these are the options of this example\n\n"
        yaml_files = info["yaml"].split("+")
        for yf in yaml_files:
            out += f".. literalinclude:: /{yf}\n"
            out += "    :language: yaml\n\n"
        # out += f'.. literalinclude:: /{info["yaml"]}\n'
        # out += '    :language: yaml\n\n'
    outname = f'{directory}/{script[script.rfind("/"):script.rfind(".py")]}.rst'
    with open(outname, "w") as f:
        f.write(out)
    print(f"Wrote file {outname}")
    return


def WriteIndex(directory, doc_directory, title, description=""):
    """Write the index page for the relevant scripts folder

    Args:
        directory (str): the folder where the scripts lay
        doc_directory (str): the name of the folder in the sphinx documentation
        title (str): the title of the page
        description (str, optional): a description of the content of the folder. Defaults to ''.
    """
    # Create documents' directory if not existing
    if not os.path.exists(doc_directory):
        os.makedirs(doc_directory)
    # Title
    out = f"{title}\n"
    out += "=" * len(title) + "\n\n"
    # Description
    if description != "":
        out += description + "\n\n"
    # Search for scripts in the directory
    valid_scripts = []
    valid_folders = []
    missingDoc = []
    for f in os.listdir(directory):
        if not f.endswith(".py"):
            if os.path.isdir(f"{directory}/{f}"):
                if not os.path.exists(f"{doc_directory}/{f}"):
                    os.makedirs(f"{doc_directory}/{f}")
                missingDoc += WriteIndex(
                    f"{directory}/{f}", f"{doc_directory}/{f}", f.capitalize()
                )
                valid_folders += [f"{doc_directory}/{f}"]
            continue
        if f == "__init__.py":
            continue
        valid_scripts += [f"{directory}/{f}"]
    valid_scripts.sort()  # sort the list to order examples and tutorials
    # toctree
    if len(valid_scripts):
        wroteTocTree = False
        if len(valid_folders) and not wroteTocTree:
            out += ".. toctree::\n\n"
            wroteTocTree = True
        for vf in valid_folders:
            # remove the first folder of the path since the file lives there
            out += f'   {vf[vf.find("/")+1:]}/{vf[vf.rfind("/")+1:]}_index\n'
        for sc in valid_scripts:
            info = GetPageInfo(sc)
            if info["title"] is None:
                missingDoc += [sc]
                warnings.warn(f"File {sc} is missing the necessary documentation!")
                continue
            if not wroteTocTree:
                out += ".. toctree::\n\n"
                wroteTocTree = True
            WritePage(sc, info, doc_directory)
            out += f'   {sc[sc.rfind("/")+1:].replace(".py","")}\n'
    # Write filename
    outname = f'{doc_directory}/{doc_directory[doc_directory.rfind("/")+1:]}_index.rst'
    with open(outname, "w") as f:
        f.write(out)
    print(f"Wrote file {outname}")
    return missingDoc


def main():
    missingDoc = []
    missingDoc += WriteIndex(
        "../DaVinciExamples/python/DaVinciExamples", "examples", "DaVinci Examples", ""
    )
    missingDoc += WriteIndex(
        "../DaVinciTutorials/python/DaVinciTutorials/",
        "tutorials",
        "DaVinci Tutorials",
        "",
    )
    if len(missingDoc):
        raise ValueError(
            "\033[91mThe following files are missing documentation:\n"
            + "\n".join(missingDoc)
            + "\033[0m"
        )
    return


# -------------------------------
if __name__ == "__main__":
    main()
