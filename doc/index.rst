Welcome to DaVinci's documentation!
===================================

DaVinci is the LHCb offline analysis application.
It allows the users to produce the tuples in which the relevant information
of the reconstructed particles of the decay of interest are stored.
Consider it as your way to access LHCb data!

The main purpose of DaVinci is tupling.
You can use it in `Analysis Productions <https://gitlab.cern.ch/lhcb-datapkg/AnalysisProductions>`__,
for submitting your own productions with `Ganga <https://ganga.readthedocs.io/en/latest/>`__,
or for running small jobs on cvmfs systems, like lxplus at CERN.

Nevertheless, since it gives you access to more detailed information about
the reconstructed particles, DaVinci also allows to perform more detailed
studies on the Sprucing or Turbo output.

This site documents the various aspects of DaVinci, which is fundamentally a group
of Python packages that configure algorithms, tools, data flow, and control
flow in order to run a `Gaudi`_-based application that has full access to Sprucing
and Turbo output.

.. _Gaudi: https://gitlab.cern.ch/gaudi/Gaudi

.. toctree::
   :caption: User Guide
   :maxdepth: 3

   configuration/davinci_configuration
   guide/funtuple
   guide/running
   guide/developing
   guide/documentation

.. toctree::
   :caption: Examples & Tutorials
   :maxdepth: 3

   tutorials/tutorials_index
   examples/examples_index

.. toctree::
   :caption: API Reference
   :maxdepth: 3

   autoapi/DaVinci/index
   davinci/options
   framework/pyconf

..
   davinci/api_index
   framework/pyconf


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
