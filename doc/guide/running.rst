Running DaVinci
===============

From ``DaVinci/v62r0`` the DaVinci configuration has been modernized and revisited in order to improve
the user accessibility and hide all technicalities the user doesn't need to deal with.
The two major changes with respect to the old configuration are:

* the general structure 'À la' PyConf
* the requirement to use ``lbexec`` (see the `talk from the 104th LHCb week for details <https://indico.cern.ch/event/1160084/#249-replacing-gaudirunpy-with>`__)

The configuration of your job is now declared in two files:

* A Python function that takes an ``options`` returns the PyConf configuration
* A YAML file which declares data specific configuration and which is used to populate the ``options`` object

DaVinci can then be run using:

.. code-block:: bash

    lb-run DaVinci/vXrY lbexec my_module:my_function options.yaml

Replacing ``lb-run DaVinci/vXrY`` with a specific version, or in the case of development builds replacing it with with the ``./run`` script.


Minimal example
---------------

Make a file named ``my_module.py`` that contains a function that takes an ``options`` argument and returns the result of ``DaVinci.make_config```:

.. code-block:: python

    from DaVinci import Options, make_config
    from DaVinci.algorithms import create_lines_filter
    from PyConf.Algorithms import PrintDecayTree
    from Davinci.reco_ocjects import get_particles


    def print_decay_tree(options: Options):
        turbo_line = "Hlt2BsToJpsiPhi_JPsi2MuMu_PhiToKK_Line"
        input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")

        user_algorithms = [
            create_lines_filter("HDRFilter_SeeNoEvil", lines =[ f"{turbo_line}"]),
            PrintDecayTree(name="PrintBsToJpsiPhi", Input=input_data)
        ]

        return make_config(options, user_algorithms)

Also make a file named ``options.yaml`` containing:


.. code-block:: yaml

    input_files:
    - root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/hlt2_passthrough_thor_lines.dst
    input_manifest_file: root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/hlt2_passthrough_thor_lines.tck.json
    input_type: ROOT
    evt_max: 100
    ntuple_file: davinci_ntuple.root
    input_process: TurboPass
    print_freq: 1
    data_type: Upgrade
    simulation: true
    conddb_tag: sim-20180530-vc-md100
    dddb_tag: dddb-20180815

This example can then be run using:

.. code-block:: bash

    lb-run DaVinci/vXrY lbexec my_module:print_decay_tree options.yaml

For a more detailed explanation of this job, as well as many more examples, see the `DaVinci tutorials repository <https://gitlab.cern.ch/lhcb/DaVinci/-/tree/master/DaVinciTutorials>`__.


Options YAML
------------

The full schema with which the ``options.yaml`` file is parsed can be found in :class:`~DaVinci.Options`.


Running in Ganga
----------------

Ganga does not support ``lbexec``, yet! A workaround has been prepared, as follows:

1. Add this to the end of your DaVinci options file e.g. ``my_module.py``

.. code-block:: python

    import sys
    if sys.argv[0].endswith("gaudirun.py"):
        import yaml
        from DaVinci import Options
        with open("options.yaml", "r") as f:
            options = Options(**yaml.safe_load(f))
        with options.apply_binds():  # required as gaudirun.py does not apply binds!
            print_decay_tree(options)  # make sure to rename print_decay_tree to whatever your function is called

The function `apply_binds()`, a member function of the LbExec `Options` class [https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/GaudiConf/python/GaudiConf/LbExec/options.py](LbExec/options.py), is automatically called when running DaVinci through `lbexec`. When running through `gaudirun.py` the binds must be applied within the DaVinci options, otherwise DaVinci will encounter reconstruction issues.

2. Add ``my_module.py`` to your ``GaudiExec`` application ``options`` list, and ``options.yaml`` to your ``job.inputfiles`` (input sandbox.)

.. code-block:: python

    j = Job()
    j.backend = Dirac()  # as required
    j.application = GaudiExec(
        dir="DaVinci_v62r0",
        platform="x86_64_v3-centos7-gcc11-opt+g"
    )
    j.application.options = ["my_module.py"]
    j.inputdata = ...  # as required
    j.inputfiles = ["hlt2.tck.json", "options.yaml"]  # change as required
    j.outputfiles = [DiracFile("*.root"), LocalFile("stdout")]  # change as required

    # ... configure as required ...

    j.submit()



3. Submit your jobs as usual. Note that in ``options.yaml``, ``input_files`` should be set to a dummy value, as it is overwritten later.
