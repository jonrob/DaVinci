Developing DaVinci
==================

There are various ways to work on `DaVinci`.
If you plan to make continuous development work, the simplest is probably by building on top of the whole stack using `lb-stack-setup <https://gitlab.cern.ch/rmatev/lb-stack-setup>`_.

.. code-block:: bash

    source /cvmfs/lhcb.cern.ch/lib/LbEnv
    curl https://gitlab.cern.ch/rmatev/lb-stack-setup/raw/master/setup.py | python3 - stack
    make DaVinci


This will require approximately 7GB of disk, but will guarantee to be up-to-date with all the dependencies.

Alternatively, especially for small changes, a development environment can be built with the commands

.. code-block:: bash

    lb-dev --name DaVinciDev DaVinci/latest
    cd DaVinciDev
    git lb-use DaVinci


Coding conventions, style and type hints
----------------------------------------

Python code in the stack follows the `PEP8 guidelines <https://peps.python.org/pep-0008/>`_
and other general good practices:

* Include comments and documentation ("docstrings") when the intent of a line, function, class, or module is not obvious.
* Factor code into functions when there is repetition and/or for clarity.
* When modifying an existing file, following the conventions of the surrounding code.

Style and formatting checks are implemented in continuous integration (see subsection below)
via  `pre-commit <https://pre-commit.com/>`_ hooks.
The following hooks are implemented: `Black`, `codespell`, `Flake8`, `pre-commit-hooks`, `pyupgrade`.
Refer to the repository `.pre-commit-config.yaml` for the actual implementation details.

Developers are encouraged to run the hooks locally prior to pushing to the repositories.
This is easily done with the following:

.. code-block:: bash

    pre-commit install  # only needed once to install pre-commit
    pre-commit run --all-files  # example to run all hooks on all files

Static typing (hints) is available in Python 3 and the offline stack now implements type hints and checking thereof.
Type hints are for now not compulsory but are strongly encouraged.


Tests and continuous integration
--------------------------------

Whenever a new commit is made to the DaVinci repository, a `CI pipeline <https://docs.gitlab.com/ee/ci/>`_ runs that performs some style and syntax checks:

 - The LHCb copyright statement should be present at the top of every source file.
 - The LHCb Python formatting rules must be adhered to.
 - The Python code must have valid syntax and not raise any `flake8 error codes <https://flake8.pycqa.org/en/2.5.5/warnings.html>`_.


Debugging
---------


Running qmt files locally
-------------------------

Various tests are implemented in the form of quality management test (qmt) files. These test can be run locally using the `qmtexec` command, for example:

.. code-block:: bash

    qmtexec DaVinciExamples/tests/qmtest/tupling.qms/test_davinci_tupling_from_data.qmt
