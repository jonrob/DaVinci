Documenting DaVinci
===================

This documentation is written in `reStructuredText`_ and built in to web pages by `Sphinx`_.
The source can be found in the `doc`_ directory of the DaVinci project.

We follow the `Google style guide`_ (`example`_) when writing `docstrings`_ in Python.

Building the documentation locally
----------------------------------

It is enough to run the following command to get the documentation in ``DaVinci/doc/_build/html``

.. code-block:: sh

 run make -C DaVinci/doc html
 run make -C DaVinci/doc linkcheck

.. _reStructuredText: https://docutils.sourceforge.io/rst.html
.. _Sphinx: https://www.sphinx-doc.org/en/master/
.. _doc: https://gitlab.cern.ch/lhcb/DaVinci/-/tree/master/doc
.. _Google style guide: https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
.. _docstrings: https://www.python.org/dev/peps/pep-0257/#what-is-a-docstring
.. _example: https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html#example-google
