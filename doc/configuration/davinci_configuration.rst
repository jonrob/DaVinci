DaVinci Configuration
=====================

Creating an ntuple
------------------

A basic example of creating a ntuple with FunTuple is:

.. literalinclude:: ../../DaVinciExamples/python/DaVinciExamples/tupling/basic.py
   :language: python
   :start-at: import

This example can be run using ``lbexec`` with the following ``options.yaml`` file:

.. code-block:: yaml

    input_files: root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076720/0000/00076720_000000{02,04,43,68}_1.ldst
    input_type: ROOT
    input_raw_format: 4.3
    data_type: Upgrade
    simulation: true
    dddb_tag: dddb-20171126
    conddb_tag: sim-20171127-vc-md100
    ntuple_file: basic-funtuple-example.root
    evt_max: 10


How to add an event pre-filter
------------------------------

Event unpacking, FunTuple and many other algorithms run in a job can be very time consuming, so the processing of
the input files could be much faster and efficient if an event pre-filter is applied on top of the whole job chain
selecting only the events that satisfy a specific set of conditions.
For adding an event per-filter in a job one can set the value of the ``evt_pre_filters`` property with a dict
containing an element for each filter you want to implement, where the key is the filter name and the object is a string
corresponding to the filter line name. Then DaVinci will automatically create the filter instance.
For example, the filter can ensure the trigger/sprucing line fired in that specific event.
This is done by including the following in your ``options.yaml`` file:

.. code-block:: yaml

   evt_pre_filters:
      Hlt2TopoLineFilter: "Hlt2Topo2BodyLineDecision"

A filter can also be instantiated directly using the ``create_lines_filter`` function:

.. code-block:: python

   from DaVinci.algorithms import create_lines_filter
   filter = create_lines_filter("Hlt2TopoLineFilter", lines=["Hlt2Topo2BodyLineDecision"]).

Additional examples can be found `here <https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciTests/tests/options/option_davinci_filters.py>`_ and `here <https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciExamples/python/DaVinciExamples/tupling/option_davinci_tupling_from_spruce.py>`__.
Additional information on how to implement a filter code can be found `here <https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/DaVinciFAQ#How_to_process_the_stripped_DSTs>`__ and `here <https://gitlab.cern.ch/lhcb/Phys/blob/master/Phys/PhysConf/python/PhysConf/Filters.py>`__.

Using the configured_FunTuple wrapper
-------------------------------------

The ``configured_FunTuple`` wrapper has been implemented to make easier the configuration of a FunTuple algorithm instance in DaVinci, combining in a single step the instantiation of different objects:

#. ``LoKi__HDRfilter``: to select only the events passing a specific set of trigger lines,
#. ``make_data_with_FetchDataFromFile``: to obtain the correct DataHandle object needed by FunTuple,
#. ``Funtuple``: object containing all the branches and variables defined by the user.

Thanks to this wrapper the user can instantiate in the user file all three objects in the following way:

.. literalinclude:: ../../DaVinciExamples/python/DaVinciExamples/tupling/option_davinci_configFuntuple.py
   :language: python
   :start-at: import

``configured_FunTuple`` takes as input a dictionary containing an entry for each FunTuple instance that has to be created,
where the the two elements are the FunTuple name and a configuration dictionary with the following information:

- ``"location"``: string with input location to be used as input to FunTuple,
- ``"filters"``: list of filters to be applied in front of FunTuple,
- ``"preamble"``: list of LoKi functors to simplify the code that is used to fill the FunTuple leaves,
- ``"tree"``: name of the FunTuple tree,
- ``"branches"``: dictionary with the FunTuple branches,
- ``"variables"``: dictionary with the FunTuple variables for each branch.

``configured_FunTuple(...)`` returns a dictionary containing lists of all the algorithms that have to be implemented for running all the defined FunTuple instances.
This wrapper is meant to be used only for the simplest cases where no other algorithms have to be added between the HDRfilter and the FunTuple;
however, it can still be used as starting point for more complex wrappers.

How to run a job on an XGEN file
--------------------------------

The DaVinci application can also be run over an XGEN (extended generator) file by setting two keys in the ``options.yaml`` file:

* Setting the ``input_type: ROOT``

The new FunTupleMC algorithm is used to create the tuple.
A working example can be found `here <https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciExamples/python/DaVinciExamples/tupling/option_davinci_tupling_from_xgen.py>`__.
This example can be run with::

    lb-run DaVinci/vXrY lbexec DaVinciExamples.tupling.option_davinci_tupling_from_xgen:main '$DAVINCIEXAMPLESROOT/example_data/Gauss_12143001_xgen.yaml'

Running over old simulation files produced using `Brunel`
=========================================================

For Run 3 data-taking, the `Moore` application is used for the offline reconstruction, a job that the `Brunel` application was responsible for during the Run 1 and Run 2 periods.
During the writing of this section (Nov 2022), majority of the simulation samples used for Run 3 upgrade studies in the bookkeeping are old and are produced using `Brunel` (note that the majority of the upgrade simulation samples in the `TestFileDB <https://gitlab.cern.ch/lhcb-datapkg/PRConfig/-/blob/master/python/PRConfig/TestFileDB.py>`__ are new and produced using `Moore`).
Therefore, care needs to be taken when running Run 3 DaVinci software with the old simulation files as input.
Particularly, to retrieve reconstructed objects one needs to use the functions defined in `RecoConf.reconstruction_objects <https://gitlab.cern.ch/lhcb/Moore/-/blob/1aa6b8ecae4e5d27d9b535a5a9cb8851046f4e03/Hlt/RecoConf/python/RecoConf/reconstruction_objects.py>`__, for example `make_pvs()` that retrieves the reconstructed primary vertices.
This is in contrast to running over the new simulation samples produced using `Moore`, where the `get_pvs()` function defined in `PyConf.reading <https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/PyConf/python/PyConf/reading.py>`__ must be used to retrieve the reconstructed primary vertices.
Another example includes combining reconstructed tracks and clusters in DaVinci to build composite particles, where in case of old simulation files one makes use of the combiners defined in `Hlt2Conf.standard_particles <https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/standard_particles.py>`__ (e.g. `make_detached_mumu` that to builds the di-muion candidates).
These same combiners can also be used when running over the new simulation files, however right inputs from `PyConf.reading <https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/PyConf/python/PyConf/reading.py>`__ need to be passed to them (if in doubt please ask the experts on the `DPA WP3 mattermost channel` <https://mattermost.web.cern.ch/lhcb/channels/dpa-wp3-offline-analysis-tools>`__).
