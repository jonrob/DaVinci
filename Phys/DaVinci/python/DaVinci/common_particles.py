################################################A##############################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definitions of "common particles" very similar to those of Runs 1 & 2.
"""
from GaudiKernel.SystemOfUnits import MeV  # type: ignore[import]
import Functors as F  # type: ignore[import]
from Functors.math import in_range  # type: ignore[import]
from PyConf.tonic import configurable  # type: ignore[import]
from PyConf.Algorithms import FunctionalParticleMaker  # type: ignore[import]
from Hlt2Conf.standard_particles import (  # type: ignore[import]
    make_long_kaons,
    make_long_muons,
    standard_protoparticle_filter,
    get_long_track_selector,
)
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner  # type: ignore[import]
from PyConf.reading import (  # type: ignore[import]
    get_charged_protoparticles as _make_charged_protoparticles,
)
from RecoConf.reco_objects_from_file import make_pvs as _make_pvs_v2  # type: ignore[import]
from DaVinci.filter_selectors import default_particle_cuts

####################################
# Particle makers with loose cut
####################################


def _make_std_loose_particles(particles, pvs, name):
    return ParticleFilter(
        Input=particles, name=name, Cut=F.FILTER(default_particle_cuts(pvs))
    )


@configurable
def make_std_loose_kaons():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return _make_std_loose_particles(
            make_long_kaons(), _make_pvs_v2(), name="StdLooseKaons"
        )


def make_std_loose_muons():
    return _make_std_loose_particles(
        make_long_muons(), _make_pvs_v2(), name="StdLooseMuons"
    )


@configurable
def make_std_loose_jpsi2mumu():
    M_Jpsi = 3096.9 * MeV
    muons = make_std_loose_muons()
    descriptor = "J/psi(1S) -> mu+ mu-"
    combination_code = (
        in_range(M_Jpsi - 100.0 * MeV, F.MASS, M_Jpsi + 100.0 * MeV)
    ) & F.MAXDOCACHI2CUT(30.0)
    vertex_code = F.CHI2 < 5.0

    return ParticleCombiner(
        name="StdLooseJpsi2MuMu",
        Inputs=[muons, muons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_std_loose_d2kk():
    D0_M = 1864.84 * MeV  # +/- 0.05
    kaons = make_std_loose_kaons()
    descriptor = "D0 -> K+ K-"
    combination_code = (
        in_range(D0_M - 100.0 * MeV, F.MASS, D0_M + 100.0 * MeV)
    ) & F.MAXDOCACHI2CUT(30.0)
    vertex_code = F.CHI2 < 25.0

    return ParticleCombiner(
        name="StdLooseD02KK",
        Inputs=[kaons, kaons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


# Temporary function implemented for testing the MAP_ARRAY functor and ParticleTaggerAlg algorithm
# in DaVinciExamples.tupling.test_davinci_tupling_array_taggers.qmt.
# Aim: create long pions particles from Spruce TES location since the standard '/Event/pRec',
# used in all the other make functions, is not available.
# TO BE REMOVED AS SOON AS THIS PYTHON MODULE IS MOVED INTO ANOTHER SHARED REPO OR
# IT'S REDESIGNED SPECIFICALLY FOR DAVINCI.
@configurable
def make_long_pions():
    long_protos = _make_charged_protoparticles(track_type="Long")
    particles = FunctionalParticleMaker(
        InputProtoParticles=long_protos,
        ParticleID="pion",
        TrackPredicate=get_long_track_selector(),
        ProtoParticlePredicate=standard_protoparticle_filter(),
    ).Particles
    return particles
