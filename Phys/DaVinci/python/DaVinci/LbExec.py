###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from contextlib import contextmanager
from typing import Optional
from GaudiConf.LbExec import Options as DefaultOptions, InputProcessTypes  # type: ignore[import]
from pydantic import root_validator  # type: ignore[import]
from PyConf.reading import upfront_decoder, reconstruction  # type: ignore[import]
from PyConf.reading import get_tes_root  # type: ignore[import]
from PyConf.application import default_raw_event  # type: ignore[import]
from PyConf.packing import persistreco_version  # type: ignore[import]
import logging


class Options(DefaultOptions):
    """
    A class that holds the user-specified DaVinci options.

    This class inherits from the default `GaudiConf.LbExec.Options`.

    This class also configures several PyConf functions,
    see the list in the `apply_binds` method,
    where their keyword arguments are globally bound to the user-specified values.
    This way, users do not have to manually configure these functions themselves.

    The optional parameters that need to be set are :
    - input_stream (str): Stream name. Default is "default" (Note: for `input_process=Hlt2` the stream must be strictly empty. The default value is overwritten in this case.)
    - lumi (bool): Flag to store luminosity information. Default is False.
    - evt_pre_filters (dict[str,str]): Event pre-filter line name(s). Default is None.
    - write_fsr (bool): Flag to write file summary record. Default is True.
    - merge_genfsr (bool): Flag to merge the file summary record. Default is False.
    - metainfo_additional_tags: (list): Additional central tags for `PyConf.filecontent_metadata.metainfo_repos`. Default is [].
    """

    input_stream: Optional[str] = "default"
    lumi: bool = False
    evt_pre_filters: Optional[dict[str, str]] = None
    write_fsr: bool = True
    merge_genfsr: bool = False
    metainfo_additional_tags: Optional[list] = []

    @root_validator(pre=False)
    def _stream_default(cls, values):
        """
        This is a validator that sets the default "stream" value based on "input_process"

        Args:
          values (dict): User-specified attributes of the Options object.

        Returns:
          dict: Modified attributes of the Options object.
        """
        input_process = values.get("input_process")
        input_stream = values.get("input_stream")
        if (
            input_process not in {InputProcessTypes.Spruce, InputProcessTypes.TurboPass}
            and input_stream != ""
        ):
            logging.getLogger(__name__).warning(
                f"input_stream is set to '{input_stream}', but will be reset to '' because current input_process = {input_process}"
            )
            values["input_stream"] = ""

        return values

    @contextmanager
    def apply_binds(self):
        """
        This function configures the following PyConf functions, where their keyword
        arguments are globally bound to the user-specified values:
        - default_raw_event
        - default_raw_banks
        - upfont_decoder
        - reconstruction
        - get_tes_root
        This way users do not have to manually configure these functions themselves.
        """
        default_raw_event.global_bind(
            raw_event_format=self.input_raw_format, stream=self.input_stream
        )
        upfront_decoder.global_bind(
            source=InputProcessTypes(self.input_process).sourceID()
        )
        reconstruction.global_bind(input_process=self.input_process)
        get_tes_root.global_bind(input_process=self.input_process)
        persistreco_version.global_bind(version=self.persistreco_version)
        with super().apply_binds():
            yield
