###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest

from PyConf.Algorithms import Gaudi__Examples__VoidConsumer as VoidConsumer

from DaVinci.config import DVNode, prepare_davinci_nodes

from pathlib import Path

DIR = Path(__file__).parent.resolve()


def test_DVNode():
    """
    Basic test of the DVNode class.
    """
    node = DVNode("MyAlgs", [VoidConsumer()])

    assert node.name == "MyAlgs"


def test_prepare_davinci_nodes():
    """
    Preparation of a minimalistic node with a single algorithm (has to be passed as a list).
    """
    list_of_algs = {"alg": [VoidConsumer()]}
    prepare_davinci_nodes(list_of_algs)


def test_prepare_davinci_nodes_TypeError():
    """
    Check that DaVinci raises an error when trying to prepare a node with a set of algorithms different from a list.
    """
    list_of_algs = {"alg": VoidConsumer()}
    with pytest.raises(TypeError):
        prepare_davinci_nodes(list_of_algs)
