###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinci
------------
#]=======================================================================]

gaudi_install(PYTHON)

lhcb_env(SET DAVINCIOPTS ${CMAKE_CURRENT_SOURCE_DIR}/options)

gaudi_add_tests(QMTest)
gaudi_add_tests(pytest ${CMAKE_CURRENT_SOURCE_DIR}/tests)
