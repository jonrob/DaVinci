###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
rst_title: 1. Special Functors Fields
rst_description: This tutorial shows how to add the same functors to all the fields in FunTuple by using the special field `ALL`.
It can be split in few steps:

1. Definition of a dictionary of `{"field name" : "decay descriptor component"}`.

  - For particle properties, names, etc checkout `ParticleTable.txt` that can be obtained via command

    .. code-block:: bash

        $DVPATH/run dump_ParticleProperties -t Upgrade | tee ParticleTable.txt

  - For decay descriptor info see for example `LoKiNewDecayFinders <https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders>`_
    If your decay is self-tagged (which is the most common case) then you will need "[<decay-descriptor>]CC"

2. Define a collection of functors called FunctorCollection, which takes dictionary of "variable name" -> "ThOr" functor (Can also be a "LoKi" functor see next tutorial).
   For more info on ThOr see the `ThOr functors page <https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors.html#functor-cache>`_
   For list of ThOr functors see the `ThOr functors reference <https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors_reference.html>`_
   In this example a functor collection is added to "ALL" fields (Bs, Jpsi, Phi, etc)
3. Define functors to be added only to Bs and Jpsi fields
4. Use the "ALL" special field name
5. Inspect the string representation of ThOr Functor. This string representation is converted to a C++ object using gcc or `FunctorCache <https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors.html#functors-in-a-selection-framework>`_
6. Finally there are the definition of the TES location, of a filter and the FunTuple instance.

rst_running: lbexec DaVinciTutorials.tutorial1_functors_specialfield:main $DAVINCITUTORIALSROOT/options.yaml
rst_yaml: ../DaVinciTutorials/options.yaml
"""
import Functors as F
from DaVinci import Options, make_config
from DaVinci.algorithms import create_lines_filter
from PyConf.reading import get_particles
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple


def main(options: Options):
    # Define a dictionary of "field name" -> "decay descriptor component".
    # - For particle properties, names, etc checkout "ParticleTable.txt"
    #   that can be obtained via command "$DVPATH/run dump_ParticleProperties -t Upgrade | tee ParticleTable.txt".
    # - For decay descriptor info see for example https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
    #   If your decay is self-tagged (which is the most common case) then you will need "[<decay-descriptor>]CC"
    fields = {
        "Bs": "B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "Jpsi": "B_s0 -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "Phi": "B_s0 ->  (J/psi(1S) -> mu+ mu-) ^(phi(1020) ->K+ K-)",
        "Mup": "B_s0 ->  (J/psi(1S) ->^mu+ mu-) (phi(1020) ->K+ K-)",
        "Mum": "B_s0 ->  (J/psi(1S) -> mu+ ^mu-) (phi(1020) ->K+ K-)",
        "Kp": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->^K+ K-)",
        "Km": "B_s0 ->  (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ ^K-)",
    }

    # Define a collection of functors called FunctorCollection, which takes dictionary of "variable name" -> "ThOr" functor
    # (Can also be a "LoKi" functor see next tutorial).
    # For more info on ThOr see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors.html#functor-cache
    # For list of ThOr functors see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors_reference.html
    # Here we define functor collection to be added to "ALL" fields (Bs, Jpsi, Phi, etc)
    all_vars = FunctorCollection(
        {
            "THOR_P": F.P,  # ThOr momentum functor
            "ID": F.PARTICLE_ID,  # Refer to "ParticleTable.txt" for particle ID (see above on how to get this file)
        }
    )

    # Define functors to be added only to Bs and Jpsi fields
    bs_jpsi_fun = FunctorCollection({"PT_THOR": F.PT, "PX": F.PX, "PY": F.PY})

    # Define variables dictionary "field name" -> Collections of functor.
    # "ALL" is a special field name that adds PT to all the fields defined above (i.e. Bs,Jpsi,Mup,Mum,Kp,Km)
    variables = {
        "ALL": all_vars,
        "Bs": bs_jpsi_fun,
        "Jpsi": bs_jpsi_fun,
    }

    # Inspect string representation of ThOr Functor
    # This string representation is converted to C++ object
    # using gcc or FunctorCache see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors.html#functors-in-a-selection-framework
    print(F.PT.code())
    # print(F.PT.headers())
    print(F.PT.code_repr())

    # Define the TES location (see previous example for explanation)
    turbo_line = "Hlt2B2CC_BsToJpsiPhi_Detached"
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")

    # Define a filter (see previous example for explanation)
    my_filter = create_lines_filter("HDRFilter_SeeNoEvil", lines=[f"{turbo_line}"])

    # Define instance of FunTuple
    mytuple = Funtuple(
        "TDirectoryName",
        "TTreeName",
        # dictionary of particle : decay descriptor
        fields=fields,
        # dictionary of particle : variables to insert in TTree
        variables=variables,
        inputs=input_data,
    )

    config = make_config(options, [my_filter, mytuple])
    return config
