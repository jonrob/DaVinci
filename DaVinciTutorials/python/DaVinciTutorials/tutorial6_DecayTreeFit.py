###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
r"""
rst_title: 6. Decay Tree Fitter
rst_description: This tutorial shows how to fit a decay tree chain with DecayTreeFitter (DTF).
DTF allows you to fit the entire decay chain and correct the momenta of the final state particles to account for the constraints of the decay chain of interest.
A typical situation involves adding a Primary Vertex or a Mass constraint.
This is dealt in FunTuple by building a relation table between the candidate and its refitted chain.
This relation table is stored in the TES location `DTF.Algorithm.OutputRelations`.

In this tutorial one can see how to apply

- a or multiple mass constraint
- a primary vertex constraint
- a combination of the above

You can apply functors to refitted candidates by simply invoking the `__call__` function (see below).

**N.B.**: In this example the :math:`J/\psi` mass constraint is applied but the :math:`\phi(1020)` mass constraint is not.
This is expected since Decay Tree Fitter takes into account the intrinsic mass resolution of the resonances when applying mass constraint.
Since the :math:`\phi(1020)` width is larger than the intrinsic LHCb mass resolution, no constraint is applied.
For more details see this `issue <https://gitlab.cern.ch/lhcb/Rec/-/issues/309>`_

For more information on DTF see:

   - the original `article <https://inspirehep.net/literature/679286>`_
   - the `LHCb page <https://twiki.cern.ch/twiki/bin/view/LHCb/DecayTreeFitter>`_
   - a lecture from its `author <https://www.nikhef.nl/~wouterh/topicallectures/TrackingAndVertexing/part6.pdf>`_

rst_running: lbexec DaVinciTutorials.tutorial6_DecayTreeFit:main $DAVINCITUTORIALSROOT/options.yaml
rst_yaml: ../DaVinciTutorials/options.yaml
"""
import Functors as F
from DaVinci import Options, make_config
from DaVinci.algorithms import create_lines_filter
from PyConf.reading import get_particles, get_pvs, get_pvs_v1

from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC


def main(options: Options):
    """
    For more information on DTF see:
    - https://inspirehep.net/literature/679286
    - https://twiki.cern.ch/twiki/bin/view/LHCb/DecayTreeFitter
    - https://www.nikhef.nl/~wouterh/topicallectures/TrackingAndVertexing/part6.pdf
    """
    from DecayTreeFitter import DecayTreeFitter

    # Define a dictionary of "field name" -> "decay descriptor component".
    fields = {
        "Bs": "B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) ->K+ K-)",
        "Jpsi": "B_s0 ->^(J/psi(1S) -> mu+ mu-)  (phi(1020) ->K+ K-)",
        "Phi": "B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) ->K+ K-)",
    }

    # Load data from dst onto a TES (See Example7)
    turbo_line = "Hlt2B2CC_BsToJpsiPhi_Detached"
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")

    # get kinematic functors
    kin = FC.Kinematics()

    ####### Mass constraint
    # For DecayTreeFitter, as with MC Truth algorithm (previous example), this algorithm builds a relation
    # table i.e. one-to-one map b/w B candidate -> Refitted B candidate.
    # The relation table is output to the TES location "DTF.Algorithm.OutputRelations"
    # You can apply functors to refitted candidates by simply invoking the __call__ function (see below).
    # Note: the Jpsi constraint is applied but the phi constraint seems not to be applied (see issue: https://gitlab.cern.ch/lhcb/Rec/-/issues/309)
    DTF = DecayTreeFitter(
        name="DTF", input_particles=input_data, mass_constraints=["J/psi(1S)"]
    )

    # Loop over the functors in kinematics function and create a new functor collection
    dtf_kin = FunctorCollection(
        {"DTF_" + k: DTF(v) for k, v in kin.get_thor_functors().items()}
    )
    # print(dtf_kin)
    #########

    ####### Mass constraint + primary vertex constraint
    # Load PVs onto TES from data. Note here that we call "make_pvs_v1()" to pass to DTF algorithm and "make_pvs()" is passed to ThOr functors.
    # The function "make_pvs()" returns v2 vertices whereas "make_pvs_v1()" returns v1 vertices.
    # The PV constraint in the Decay tree fitter currently only works with v1
    # (see https://gitlab.cern.ch/lhcb/Rec/-/issues/318 and https://gitlab.cern.ch/lhcb/Rec/-/issues/309)
    pvs = get_pvs_v1()
    pvs_v2 = get_pvs()

    # Add not only mass but also constrain Bs to be coming from primary vertex
    DTFpv = DecayTreeFitter(
        "DTFpv",  # name of algorithm
        input_data,  # input particles
        input_pvs=pvs,
        mass_constraints=["J/psi(1S)", "phi(1020)"],
    )

    # define the functors
    pv_fun = {}
    pv_fun["BPVLTIME"] = F.BPVLTIME(pvs_v2)
    pv_fun["BPVIPCHI2"] = F.BPVIPCHI2(pvs_v2)
    pv_coll = FunctorCollection(pv_fun)

    # We now take the pre-defined functor collection ("pv_fun") and add same variables to it
    # but using the result of the decay tree fit (DTF). These variables will have the prefix ("DTFPV_").
    # The resolution on the B candidate lifetime post-DTF ("DTFPV_BPVLTIME")
    # should have improved compared to lifetime variable pre-DTF ("BPVLTIME").
    # Below we make use of the helper function ("DTFPV_MAP") defined previously.
    pv_coll += FunctorCollection(
        {"DTFPV_" + k: DTFpv(v) for k, v in pv_coll.get_thor_functors().items()}
    )

    # Define variables dictionary "field name" -> Collections of functor
    variables = {"ALL": kin + dtf_kin, "Bs": pv_coll}

    # Add a filter (See Example7)
    my_filter = create_lines_filter("HDRFilter_SeeNoEvil", lines=[f"{turbo_line}"])

    # Define instance of FunTuple
    mytuple = Funtuple(
        "TDirectoryName",
        "TTreeName",
        fields=fields,
        variables=variables,
        inputs=input_data,
    )

    config = make_config(options, [my_filter, mytuple])
    return config
