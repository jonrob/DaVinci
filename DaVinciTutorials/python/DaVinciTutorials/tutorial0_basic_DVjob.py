###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
rst_title: 0. Basic DaVinci Job
rst_description: Basic operation of DaVinci.
This tutorial shows how to read particles from a turbo line and print the decay tree of the candidate.
To improve processing speed, a pre-processing filter on the trigger line of interest is applied.

rst_running: lbexec DaVinciTutorials.tutorial0_basic_DVjob:main $DAVINCITUTORIALSROOT/options.yaml
rst_yaml: ../DaVinciTutorials/options.yaml
"""
from DaVinci import Options, make_config
from DaVinci.algorithms import create_lines_filter
from PyConf.reading import get_particles
from PyConf.Algorithms import PrintDecayTree


def main(options: Options):
    # Load data from dst onto a "temporary" TES (Transient Event Store) location for a given event cycle.
    # We loop over the algorithms event-by-event, so for given event cycle, TES maps "path" to an "object".
    # For the TES path checkout spruce_passthrough.tck.json or you can do a dst dump
    # (see https://lhcb.github.io/starterkit-lessons/first-analysis-steps/interactive-dst.html)
    #
    turbo_line = "Hlt2B2CC_BsToJpsiPhi_Detached"
    input_data = get_particles(f"/Event/HLT2/{turbo_line}/Particles")

    # Add a filter: We are not really filtering over particles
    # If the event hasn't fired a HLT2 line then the corresponding TES location does
    # obviously not exist and therefore if any algorithm tries to look for this location,
    # it will of course fail.
    # Resolve this with a filter, where:
    # - 1st argument is a user defined name.
    # - 2nd argument is the line decision (inspect hlt2_starterkit.tck.json if needed))
    my_filter = create_lines_filter("HDRFilter_SeeNoEvil", lines=[f"{turbo_line}"])

    # Defining an algorithm. The algorithm here prints the decaytree
    pdt = PrintDecayTree(name="PrintBsToJpsiPhi", Input=input_data)

    user_algorithms = [my_filter, pdt]

    return make_config(options, user_algorithms)
