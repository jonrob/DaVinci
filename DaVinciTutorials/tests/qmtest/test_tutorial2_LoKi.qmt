<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <argument name="options_yaml_fn"><text>$DAVINCITUTORIALSROOT/options.yaml</text></argument>
  <argument name="extra_options_yaml"><text>
    ntuple_file: tutorial2_LoKi.root
  </text></argument>
  <argument name="args"><set>
    <text>DaVinciTutorials.tutorial2_LoKi:main</text>
  </set></argument>
  <argument name="reference"><text>../refs/test_tutorial2_LoKi.ref</text></argument>
  <argument name="error_reference"><text>../refs/empty.ref</text></argument>
  <argument name="validator"><text>
from DaVinciTests.QMTest.DaVinciExclusions import preprocessor, counter_preprocessor

validateWithReference(preproc = preprocessor, counter_preproc = counter_preprocessor)

countErrorLines({"FATAL":0, "ERROR":0})

import os
from DaVinciTests.QMTest.check_helpers import get_pandas_dataframe, df_has_nan

ntuple  = './tutorial2_LoKi.root'
#this file should be disabled
ntuple_new  = './tutorial2_LoKi_new.root'

if not os.path.isfile(ntuple):
    causes.append(f"File {ntuple} does not exist!")

df = get_pandas_dataframe(ntuple, 'TDirectoryName/TTreeName')

# Check ntuple structure
if df.empty:
    causes.append(f"File {ntuple}: ntuple does not contain any branches")
if df.shape != (41, 50):
    causes.append("Ntuple not with expected number of entries and/or branches")

# Check there are no NaN values in the ntuple
if df_has_nan(df):
    causes.append("Ntuple contains NaN entries")

print('Test successfully completed!')
os.system(f"rm {ntuple}")
os.system(f"rm {ntuple_new}")
  </text></argument>
</extension>
