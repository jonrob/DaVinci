/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** \mainpage notitle
 *  \anchor davincidoxygenmain
 *
 * This is the code reference manual for the DaVinci Physics Analysis classes.

 * These pages have been generated directly from the code and reflect the exact
 * state of the software for this version of the DaVinciSys packages. More
 * information is available from the
 * <a href="http://cern.ch/lhcb-release-area/DOC/davinci/">web pages</a>
 * of the DaVinci project
 *
 * <b>The classes used by DaVinci are documented in the following reference manuals:</b>
 *
 * \li \ref physsysdoxygenmain "PhysSys documentation (LHCb physics classes)"
 * \li \ref analysissysdoxygenmain "AnalysisSys documentation (LHCb analysis classes)"
 * \li \ref recsysdoxygenmain "RecSys documentation (LHCb reconstruction classes)"
 * \li \ref lbcomdoxygenmain "LbcomSys documentation (LHCb shared components)"
 * \li \ref lhcbdoxygenmain  "LHCbSys documentation"
 * \li \ref gaudidoxygenmain "Gaudi documentation"
 * \li \ref externaldocs     "Related external libraries"
 * \li <a href="https://twiki.cern.ch/twiki/bin/view/LHCb/DaVinci">DaVinci wiki page</a>
 *
 * \sa
 * <a href="../release.notes"><b>DaVinci release notes history</b></a>
 *
 * <hr>
 * \htmlinclude new_release.notes
 * <hr>

 */
