###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read the output of an Sprucing job with the new DaVinci configuration.
"""
from PyConf.reading import get_particles
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config


def main(options: Options):
    bd2dsk_line = get_particles(
        "/Event/Spruce/SpruceB2OC_BdToDsmK_DsmToHHH_FEST_Line/Particles"
    )

    fields = {
        "B0": "[B0 -> D_s- K+]CC",
        "Ds": "[B0 -> ^D_s- K+]CC",
        "Kp": "[B0 -> D_s- ^K+]CC",
    }

    variables_B0 = FunctorCollection(
        {
            "LOKI_MAXPT": "TRACK_MAX_PT",
            "LOKI_Muonp_PT": "CHILD(PT, 1)",
            "LOKI_Muonm_PT": "CHILD(PT, 2)",
        }
    )

    variables_extra = FunctorCollection(
        {"LOKI_NTRCKS_ABV_THRSHLD": "NINTREE(ISBASIC & (PT > 15*MeV))"}
    )
    variables_B0 += variables_extra

    # FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = FC.Kinematics()

    # FunTuple: associate functor collections to field (branch) name
    variables = {
        "ALL": variables_all,  # adds variables to all fields
        "B0": variables_B0,
        "Ds": variables_extra,
        "Kp": variables_extra,
    }

    loki_preamble = ["TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)"]

    tuple_B0DsK = Funtuple(
        name="B0DsK_Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        loki_preamble=loki_preamble,
        inputs=bd2dsk_line,
    )

    filter_B0DsK = create_lines_filter(
        "HDRFilter_B0DsK", lines=["SpruceB2OC_BdToDsmK_DsmToHHH_FEST_Line"]
    )

    return make_config(options, [filter_B0DsK, tuple_B0DsK])
