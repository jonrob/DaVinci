###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
 Option file for testing if FunTuple handles correctly an array with a user-defined size.
 This example is meant to be run with:

    $ ./run davinci run-mc --inputfiledb Spruce_all_lines_dst Phys/DaVinci/options/DaVinciDB-Example.yaml --user_algorithms DaVinciTests/tests/options/option_davinci_funtuple_array:main
"""

import Functors as F
from PyConf.Algorithms import ParticleTaggerAlg, ParticleContainerMerger
from FunTuple import FunctorCollection, FunTuple_Particles as Funtuple
from DaVinci.algorithms import create_lines_filter
from DaVinci.common_particles import make_long_pions
from PyConf.reading import get_particles

from DaVinci import options

options.input_manifest_file = "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/spruce_all_lines_realtime_newPacking.tck.json"
options.evt_max = 100
options.histo_file = "DV-test-array-his.root"
options.ntuple_file = "DV-test-array-ntp.root"
options.input_process = "Spruce"
options.persistreco_version = 0.0

bd2dsk_line = "SpruceB2OC_BdToDsmK_DsmToHHH_FEST_Line"
bd2dsk_data = get_particles(f"/Event/Spruce/{bd2dsk_line}/Particles")

# In this test we want to save the information regarding long pions available in the event
# storing them in a set of arrays.
pions = make_long_pions()

tagging_container = ParticleContainerMerger(InputContainers=[pions]).OutputContainer
tagAlg = ParticleTaggerAlg(
    Input=bd2dsk_data, TaggingContainer=tagging_container, OutputLevel=3
)
tagAlg_rels = tagAlg.OutputRelations

# make collection of functors
variables_B = FunctorCollection(
    {
        "THOR_MASS": F.MASS,
        "TagTr_P": F.MAP_INPUT_ARRAY(Functor=F.P, Relations=tagAlg_rels),
        # Currently this stores a branch called "indx" which corresponds to nPVs.
        # You can give a custom name for this via following
        "TagTr_PT[nTags]": F.MAP_INPUT_ARRAY(Functor=F.PT, Relations=tagAlg_rels),
    }
)

# Defining fields and variables we want to store in our tuple
fields = {"B0": "[B0 -> D_s- K+]CC"}
variables = {"B0": variables_B}

tuple_B0DsK = Funtuple(
    name="B0DsK_Tuple",
    tuple_name="DecayTree",
    fields=fields,
    variables=variables,
    inputs=bd2dsk_data,
)

filter_B0DsK = create_lines_filter("HDRFilter_B0DsK", lines=[f"{bd2dsk_line}"])


def main():
    algs = [filter_B0DsK, tuple_B0DsK]
    return algs, []
