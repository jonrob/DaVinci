###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of a typical DaVinci job:
 - selection of two detached opposite-charge muons
 - tuple of the selected candidates
 - runs DecayTreeFitterAlg and stores some output

Note:
Below mass constraints are applied to J/psi(1S) and psi(2S) going to dimuon pairs
whereas the tupling is only performed for the former.
The latter constraint raises an expected warning to ensure that the DTF
does not silently ignore such spurious constraints, see https://gitlab.cern.ch/lhcb/Rec/-/issues/408.
"""
import Functors as F
from Gaudi.Configuration import INFO
from Hlt2Conf.standard_particles import make_detached_mumu
from RecoConf.reconstruction_objects import upfront_reconstruction
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from DecayTreeFitter import DecayTreeFitter
from DaVinci import Options, make_config


def main(options: Options):
    # Prepare the node with the selection
    dimuons = make_detached_mumu()

    # DecayTreeFitter Algorithm.
    # One with PV constraint and one without

    DTF = DecayTreeFitter(
        name="DTF_dimuons",
        input_particles=dimuons,
        mass_constraints=["J/psi(1S)", "psi(2S)"],
        output_level=INFO,
    )

    # FunTuple: Jpsi info
    fields = {}
    fields["Jpsi"] = "J/psi(1S) -> mu+ mu-"

    # make collection of functors for Jpsi
    variables_jpsi = FunctorCollection(
        {
            "THOR_MASS": F.MASS,
            "DTF_PT": DTF(F.PT),
            "DTF_MASS": DTF(F.MASS),
        }
    )

    # associate FunctorCollection to field (branch) name
    variables = {}
    variables["Jpsi"] = variables_jpsi

    # FunTuple: define list of preambles for loki
    loki_preamble = ["TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)"]

    # Configure Funtuple algorithm
    tuple_dimuons = Funtuple(
        name="DimuonsTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        loki_preamble=loki_preamble,
        inputs=dimuons,
    )

    return make_config(options, upfront_reconstruction() + [tuple_dimuons])
