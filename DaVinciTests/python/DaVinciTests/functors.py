###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test of functors
"""
from GaudiKernel.SystemOfUnits import MeV

import Functors as F
from PyConf.Algorithms import ParticleRangeFilter, TwoBodyCombiner
from PyConf.application import configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import PrintDecayTree
from Functors.math import in_range

from RecoConf.reconstruction_objects import upfront_reconstruction
from Hlt2Conf.standard_particles import make_long_kaons

from DaVinci import Options
from DaVinci.common_particles import make_std_loose_d2kk


def make_tight_d2kk():
    """
    Make tight D->KK
    """
    kaons = make_long_kaons()
    descriptor = "D0 -> K+ K-"
    #   todo : add a ADAMASS-like cut
    combination_code = (F.DOCACHI2(1, 2) < 30) & in_range(
        1764 * MeV, F.MASS, 1964 * MeV
    )
    vertex_code = F.CHI2DOF < 30

    print(f"### Kaons are {kaons}")

    filtered_kaons = ParticleRangeFilter(Input=kaons, Cut=F.FILTER(F.PROBNN_K > 0.5))
    p = TwoBodyCombiner(
        name="TightD02KK",
        Input1=filtered_kaons,
        Input2=filtered_kaons,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        ParticleCombiner="ParticleVertexFitter",
        CompositeCut=vertex_code,
    ).OutputParticles

    print(f"### Returning {p}")
    return p


def main(options: Options):
    vd0s = make_std_loose_d2kk()
    td0s = make_tight_d2kk()

    print(f"### vD0s {vd0s} and tD0s {td0s}")

    pdt = PrintDecayTree(name="PrintD0s", Input=vd0s)  # keyed container
    pdt2 = PrintDecayTree(name="PrintTightD0s", Input=td0s)

    # the "upfront_reconstruction" is what unpacks reconstruction objects, particles and primary vertices
    # from file and creates protoparticles.
    algs = upfront_reconstruction() + [vd0s, pdt, td0s, pdt2]

    node = CompositeNode(
        "PrintD0Node", children=algs, combine_logic=NodeLogic.NONLAZY_OR
    )

    config = configure_input(options)
    config.update(configure(options, node))
    return config
