###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTest import LineSkipper
from RecConf.QMTest.exclusions import preprocessor as RecPreprocessor

remove_uninteresting_lines = LineSkipper(
    regexps=[
        r"IODataManager +ERROR Referring to existing dataset",
        r"EventSelector +INFO Stream:EventSelector.DataStreamTool.*",
        r"MagneticFieldSvc +INFO Map scaled by factor",
        r"MagneticFieldSvc +INFO Opened magnetic field file",
        r"ConfigTarFileAccessSvc +INFO  opening",
        r"(Memory for the event|Mean 'delta-memory') exceeds 3*sigma",
        r"(AfterMagnet|Magnet|BeforeMagnet|Downstream)Region/.*",
        r"(LHCBCOND_|SIMCOND|DDDB)_",
        r"DataOnDemandSvc +INFO Handled",
        r"HLTControlFlowMgr +INFO",
        r"<module .*",
        "SUCCESS Exceptions/Errors/Warnings/Infos Statistics :",
        "INFO MuonIDAlg v",
        "DaVinciInitAlg.DaVinciMemory",
        "mc_unpackers' must run BEFORE 'unpackers'",
        "MD5 sum:",
        "DeFTDetector +INFO Current FT geometry version",
        "LHCb::Det::LbDD4hep::DD4hepSvc +INFO Field map location",
        "DeMagnetConditionCall +INFO Loading mag field from",
        "MagneticFieldExtension +INFO Scale factor",
        "ToolSvc.TrackStateProvider.Track... WARNING TransportSvc is currently incompatible with DD4HEP .*",
        "ToolSvc.TrackStateProvider.Track... WARNING See https://gitlab.cern.ch/lhcb/Rec/-/issues/326 for more details",
        "ToolSvc +INFO Removing all tools created by ToolSvc",
    ]
)

# Remove known warnings from the references
remove_known_warnings = LineSkipper(
    regexps=[
        # expected WARNINGs from the data broker
        r"HiveDataBrokerSvc +WARNING non-reentrant algorithm: .*",
        # Until tck is implemented HltPackedDataDecoder/HltDecReportsDecoder will raise warning
        r"HltPackedBufferDecoder +WARNING TCK in rawbank seems to be 0 .*",
        r"HltPackedBufferDe...WARNING TCK in rawbank seems to be 0 .*",
        r"HltDecReportsDecoder +WARNING TCK obtained from rawbank seems to be 0 .*",
        r"Hlt. +WARNING TCK obtained from rawbank seems to be 0 .*",
        r"Hlt2DecReports +WARNING TCK obtained from rawbank seems to be 0 .*",
        r"SpruceDecReports +WARNING TCK obtained from rawbank seems to be 0 .*",
        r"Spruce +WARNING TCK obtained from rawbank seems to be 0 .*",
        # Until MC tools become functional, MC tools will run but the output is nonsense
        "WARNING: MC tools not functional yet outside Hlt2 - .*",
        # FunTuple warning for the update_counters: skipping event is no candidates are found
        r"[a-zA-Z0-9]* +WARNING FunTupleBase<KeyedContainer<LHCb::MCParticle,Containers::KeyedObjectManager<Containers::hashmap> > >:: .*",
        r"DetectorPersistencySvc +INFO Added successfully Conversion service:XmlCnvSvc",
        r"DetectorDataSvc +SUCCESS Detector description database: git:/lhcb.xml",
        r"[a-zA-Z0-9]* +WARNING TransportSvc is currently incompatible with DD4HEP. .*",
        r"[a-zA-Z0-9]* +WARNING See https://gitlab.cern.ch/lhcb/Rec/-/issues/326 for more details",
        # backwards compatibility -- key is from manifest, not git
        r"key 0x[0-9a-f]+ has an explicitly configured overrule -- using that",
        # backwards compatibility -- old data
        r"DstData raw bank  has a zero encoding key, and it is not explicitly specified for decoding *",
        r"HltDecReports has a zero TCK, and it is not explicitly specified for decoding *",
    ]
)

remove_known_fluctuating_counters = LineSkipper(
    regexps=[
        # Functor cache hit/miss counters; can vary in uninteresting ways in
        # reconstruction tests based on HLT2 line content
        r' \| "# loaded from (?:CACHE|PYTHON)" .*',
        # The HltPackedDataWriter uses two data buffers: one that holds uncompressed
        # data and another that holds this data after compression. For some reason,
        # the size of the compressed data buffer fluctuates slightly (~0.001%) between runs.
        # The same does not happen for the uncompressed data buffer.
        # See https://gitlab.cern.ch/lhcb/LHCb/-/issues/151
        r' \| "Size of compressed data" .*',
    ]
)

# appears in stderr
remove_known_errors = LineSkipper(
    strings=[
        "Info in <TGeoManager>: Changing system of units to Geant4 units (mm, ns, MeV)."
    ]
)
preprocessor = (
    RecPreprocessor
    + remove_uninteresting_lines
    + remove_known_warnings
    + remove_known_errors
)
counter_preprocessor = remove_known_fluctuating_counters
