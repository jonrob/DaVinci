###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test for the function returning DataHandle for v2 RecVertices for usage in ThOr functors
"""
import Functors as F
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.reading import get_particles, get_pvs
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config


def main(options: Options):
    bd2dsk_line = "SpruceB2OC_BdToDsmK_DsmToHHH_FEST"
    bd2dsk_data = get_particles(f"/Event/Spruce/{bd2dsk_line}/Particles")

    fields_dsk = {
        "B0": "[B0 -> D_s- K+]CC",
    }

    v2_pvs = get_pvs()

    variables_pvs = FunctorCollection(
        {
            "BPVDIRA": F.BPVDIRA(v2_pvs),
            "BPVFDCHI2": F.BPVFDCHI2(v2_pvs),
            "BPVIPCHI2": F.BPVIPCHI2(v2_pvs),
        }
    )

    variables_dsk = {
        "B0": variables_pvs,
    }

    my_filter = create_lines_filter("HDRFilter_B0DsK", lines=[f"{bd2dsk_line}Decision"])
    my_tuple = Funtuple(
        name="B0DsK_Tuple",
        tuple_name="DecayTree",
        fields=fields_dsk,
        variables=variables_dsk,
        inputs=bd2dsk_data,
    )

    return make_config(options, [my_filter, my_tuple])
