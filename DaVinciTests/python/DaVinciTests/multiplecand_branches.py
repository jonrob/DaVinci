###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test for branches related to multiple candidates in an event.
 - test that the branches related to totCandidates and nCandidates are filled correctly
"""
import Functors as F
from Hlt2Conf.standard_particles import make_detached_mumu
from RecoConf.reconstruction_objects import upfront_reconstruction
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from DaVinci import Options, make_config


def main(options: Options):
    # Prepare the node with the selection
    dimuons = make_detached_mumu()

    # FunTuple: Jpsi info
    fields = {}
    fields["Jpsi"] = "J/psi(1S) -> mu+ mu-"

    # make collection of functors for Jpsi
    variables_jpsi = FunctorCollection(
        {
            "THOR_MASS": F.MASS,
        }
    )

    # associate FunctorCollection to field (branch) name
    variables = {}
    variables["Jpsi"] = variables_jpsi

    # Configure Funtuple algorithm
    tuple_dimuons = Funtuple(
        name="DimuonsTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=dimuons,
        store_multiple_cand_info=True,
    )

    return make_config(options, upfront_reconstruction() + [tuple_dimuons])
