###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
rst_title: Test for DaVinci issue 97
rst_description: Test for DaVinci issue 97 on an incorrect behaviour
of the `MCTruthAndBkgCat` helper in Analysis's `Phys/DaVinciMCTools` package,
traced back to a bug in one of the tools internally called.
rst_running: lbexec DaVinciTests.mc.option_davinci-issue-97_bkgcat_mc-truth:main "$DAVINCITESTSROOT/python/DaVinciTests/mc/option_davinci-issue-97_bkgcat_mc-truth.yaml"
rst_yaml: ../DaVinciTests/python/DaVinciTests/mc/option_davinci-issue-97_bkgcat_mc-truth.yaml
"""
import Functors as F

from PyConf.reading import get_particles

from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC

from DaVinciMCTools import MCTruthAndBkgCat
from DaVinci import make_config, Options
from DaVinci.algorithms import create_lines_filter

my_line = "Hlt2Lb2JpsiLambda"


def make_branches():
    return {
        "Lb": "[Lambda_b0 -> ( J/psi(1S) -> mu+ mu- ) ( Lambda0 -> p+ pi- )]CC",
        "Jpsi": "[Lambda_b0 -> ^( J/psi(1S) -> mu+ mu- ) ( Lambda0 -> p+ pi- )]CC",
        "Lambda0": "[Lambda_b0 -> ( J/psi(1S) -> mu+ mu- ) ^( Lambda0 -> p+ pi- )]CC",
        "mu_plus": "[Lambda_b0 -> ( J/psi(1S) -> ^mu+ mu- ) ( Lambda0 -> p+ pi- )]CC",
        "mu_minus": "[Lambda_b0-> ( J/psi(1S) -> mu+ ^mu- ) ( Lambda0 -> p+ pi- )]CC",
        "p_plus": "[Lambda_b0 -> ( J/psi(1S) ->mu+ mu- ) ( Lambda0 -> ^p+ pi- )]CC",
        "pi_minus": "[Lambda_b0 -> ( J/psi(1S) -> mu+ mu- ) ( Lambda0 -> p+ ^pi- )]CC",
    }


def variables_all():
    return FunctorCollection(
        {
            "MASS": F.MASS,
            "PT": F.PT,
            "ETA": F.ETA,
            "PHI": F.PHI,
        }
    )


def make_variables():
    return {"ALL": variables_all()}  # adds these variables to all branches


def make_trueid_bkgcat_info(mctruth):
    MCTRUTH = mctruth
    trueid_bkgcat_info = {
        "BKGCAT": MCTRUTH.BkgCat,
        "TRUEPT": MCTRUTH(F.PT),
        "TRUEORIGINVZ": MCTRUTH(F.ORIGIN_VZ),
        "TRUEORIGINVX": MCTRUTH(F.ORIGIN_VX),
        "TRUEORIGINVY": MCTRUTH(F.ORIGIN_VY),
    }
    return trueid_bkgcat_info


def main(options: Options):
    my_data = get_particles(f"/Event/HLT2/{my_line}/Particles")

    my_filter = create_lines_filter("LoKi__HDRFilter", lines=[f"{my_line}"])

    mctruth = MCTruthAndBkgCat(input_particles=my_data, name="MCTruthAndBkgCat_bkg")
    trueid_bkgcat_info = make_trueid_bkgcat_info(mctruth)

    mchierarchy_info = FC.MCHierarchy(mctruth_alg=mctruth)

    variables = make_variables()

    for branch in variables.keys():
        variables[branch] += FunctorCollection(trueid_bkgcat_info)
        variables[branch] += mchierarchy_info

    evt_variables = FC.SelectionInfo(selection_type="Hlt2", trigger_lines=[my_line])

    my_tuple = Funtuple(
        tuple_name="Tuple",
        name=f"{my_line}Tuple",
        fields=make_branches(),
        variables=variables,
        event_variables=evt_variables,
        inputs=my_data,
    )

    algs = {my_line: [my_filter, my_tuple]}

    return make_config(options, algs)
