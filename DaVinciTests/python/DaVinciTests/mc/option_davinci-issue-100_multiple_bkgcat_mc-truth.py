###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
rst_title: Test for DaVinci issue 100
rst_description: Test for DaVinci issue 100 on an incorrect behaviour
of the `MCTruthAndBkgCat` helper in Analysis's `Phys/DaVinciMCTools` package,
traced back to a bug in one of the tools internally called.
The test ensures that several instances of `MCTruthAndBkgCat`
can be used in the same script to produce several ntuples.
rst_running: lbexec DaVinciTests.mc.option_davinci-issue-100_multiple_bkgcat_mc-truth:alg_config "$DAVINCITESTSROOT/python/DaVinciTests/mc/option_davinci-issue-100_multiple_bkgcat_mc-truth.yaml"
rst_yaml: ../DaVinciTests/python/DaVinciTests/mc/option_davinci-issue-100_multiple_bkgcat_mc-truth.yaml
"""
from PyConf.reading import get_particles
import Functors as F
from FunTuple import FunctorCollection, FunTuple_Particles as Funtuple
from FunTuple.functorcollections import SelectionInfo
from DaVinciMCTools import MCTruthAndBkgCat
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config

jpsi_branches = {
    "J_psi_1S": "J/psi(1S) -> mu- mu+",
    "muminus": "J/psi(1S) -> ^mu- mu+",
    "muplus": "J/psi(1S) -> mu- ^mu+",
}

hlt2_tag_lines = [
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Tag",
]

hlt2_match_lines = [
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Match",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Match",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Match",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Match",
]


def alg_config(options: Options):
    evt_variables = SelectionInfo(
        selection_type="Hlt2", trigger_lines=hlt2_tag_lines + hlt2_match_lines
    )

    Tuples = {}
    for line in hlt2_tag_lines:
        jpsi_data = get_particles(f"/Event/HLT2/{line}/Particles")
        MC_TRUTH = MCTruthAndBkgCat(jpsi_data, name=f"MCTruthAndBkgCat_{line[-16:]}")
        MCMOTHER_ID = lambda n: F.VALUE_OR(0) @ MC_TRUTH(F.MC_MOTHER(n, F.PARTICLE_ID))
        MCMOTHER_KEY = lambda n: F.VALUE_OR(-1) @ MC_TRUTH(F.MC_MOTHER(n, F.OBJECT_KEY))

        trueid_bkgcat_info = FunctorCollection(
            {
                "TRUEID": F.VALUE_OR(0) @ MC_TRUTH(F.PARTICLE_ID),
                "TRUEKEY": F.VALUE_OR(-1) @ MC_TRUTH(F.OBJECT_KEY),
                "TRUEPT": MC_TRUTH(F.PT),
                "TRUEM": MC_TRUTH(F.MASS),
                "MC_MOTHER_ID": MCMOTHER_ID(1),
                "MC_MOTHER_KEY": MCMOTHER_KEY(1),
                "MC_GD_MOTHER_ID": MCMOTHER_ID(2),
                "MC_GD_MOTHER_KEY": MCMOTHER_KEY(2),
                "MC_GD_GD_MOTHER_ID": MCMOTHER_ID(3),
                "MC_GD_GD_MOTHER_KEY": MCMOTHER_KEY(3),
                "BKGCAT": MC_TRUTH.BkgCat,
            }
        )

        variables = {"ALL": trueid_bkgcat_info}

        jpsi_tuple = Funtuple(
            name="Tuple_" + line,
            tuple_name="DecayTree",
            fields=jpsi_branches,
            variables=variables,
            event_variables=evt_variables,
            inputs=jpsi_data,
        )

        jpsi_filter = create_lines_filter(f"Filter_{line}", lines=[f"{line}"])

        Tuples[line] = [jpsi_filter, jpsi_tuple]

    return make_config(options, Tuples)
