###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of a DaVinci job printing run and event numbers on every read event.
"""
from Gaudi.Configuration import ERROR
from PyConf.Algorithms import Gaudi__Examples__VoidConsumer as VoidConsumer
from DaVinci import Options, make_config


def main(options: Options):
    return make_config(options, [VoidConsumer(OutputLevel=ERROR)])
