###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
DaVinciTests
------------
#]=======================================================================]

gaudi_install(PYTHON)

gaudi_add_tests(QMTest)

if(BUILD_TESTING AND USE_DD4HEP)
    # Disable some tests that are not for dd4hep
    set_property(
        TEST
            DaVinciTests.mc.test_davinci-issue-97_bkgcat_mc-truth
            DaVinciTests.mc.test_davinci-issue-100_multiple_bkgcat_mc-truth
        PROPERTY
            DISABLED TRUE
    )
endif()
