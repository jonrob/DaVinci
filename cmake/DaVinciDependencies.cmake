###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
if(NOT COMMAND lhcb_find_package)
  # Look for LHCb find_package wrapper
  find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
  if(LHCbFindPackage_FILE)
      include(${LHCbFindPackage_FILE})
  else()
      # if not found, use the standard find_package
      macro(lhcb_find_package)
          find_package(${ARGV})
      endmacro()
  endif()
endif()

# -- Public dependencies
lhcb_find_package(Analysis REQUIRED)
lhcb_find_package(Moore REQUIRED)

find_data_package(AppConfig 3.0 REQUIRED)
find_data_package(FieldMap 5.0 REQUIRED)
find_data_package(ParamFiles 8.0 REQUIRED)
find_data_package(QMTestFiles 1.0 REQUIRED)
find_data_package(PRConfig 1.0 REQUIRED)
find_data_package(TCK/HltTCK REQUIRED)

# -- Private dependencies
if(WITH_DaVinci_PRIVATE_DEPENDENCIES)
    find_package(Python REQUIRED Interpreter)
endif()
