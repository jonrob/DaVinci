###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example options to read the output of a Sprucing job with the new DaVinci configuration
filtering on an HLT2 line decision.

rst_title: Use `configured_FunTuple`
rst_description: This example shows how to read the output of a Sprucing job with the new DaVinci configuration
filtering on an HLT2 line decision.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_configFuntuple:main $DAVINCIEXAMPLESROOT/example_data/Spruce_all_lines_dst.yaml
rst_yaml: ../DaVinciExamples/example_data/Spruce_all_lines_dst.yaml
"""
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC
from DaVinci.algorithms import configured_FunTuple
from DaVinci import Options, make_config


def main(options: Options):
    # FunTuple: define fields (branches)
    fields = {
        "B0": "[B0 -> D_s- pi+]CC",
        "Ds": "[B0 -> ^D_s- pi+]CC",
        "pip": "[B0 -> D_s- ^pi+]CC",
    }

    # FunTuple: define variables for the B meson
    variables_B = {
        "LOKI_MAXPT": "TRACK_MAX_PT",
        "LOKI_Muonp_PT": "CHILD(PT, 1)",
        "LOKI_Muonm_PT": "CHILD(PT, 2)",
        "LOKI_NTRCKS_ABV_THRSHLD": "NINTREE(ISBASIC & (PT > 15*MeV))",
    }

    # FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = FC.Kinematics()

    # FunTuple: associate functor collections to field (branch) name
    variables = {
        "ALL": variables_all,  # adds variables to all fields
        "B0": FunctorCollection(variables_B),
    }

    line = "SpruceB2OC_BdToDsmPi_DsmToKpKmPim"
    config = {
        "location": f"/Event/Spruce/{line}/Particles",
        "filters": [f"{line}Decision"],
        "preamble": ["TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)"],
        "tuple": "DecayTree",
        "fields": fields,
        "variables": variables,
    }

    algs = configured_FunTuple({"B0Dspi": config})

    return make_config(options, algs)
