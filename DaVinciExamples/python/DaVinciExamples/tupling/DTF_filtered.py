###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of a DaVinci job running DecayTreeFitterAlg on a filtered selection of particles

rst_title: DecayTreeFitter on filtered selection
rst_description: This example shows how to:

1. select two detached opposite-charge muons
2. create a tuple of the selected candidates
3. filter to select only the particles of interest
4. run DecayTreeFitterAlg and stores some output

rst_running: lbexec DaVinciExamples.tupling.DTF_filtered:main $DAVINCIEXAMPLESROOT/example_data/Spruce_all_lines_dst.yaml+$DAVINCIEXAMPLESROOT/example_options/example_tupling_DTF_filtered.yaml
rst_yaml: ../DaVinciExamples/example_data/Spruce_all_lines_dst.yaml+../DaVinciExamples/example_options/example_tupling_DTF_filtered.yaml
"""
import Functors as F
from Gaudi.Configuration import INFO
from DaVinci import Options, make_config
from DaVinci.algorithms import create_lines_filter
from DecayTreeFitter import DecayTreeFitter
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.reading import get_particles


def main(options: Options):
    # fields for FunTuple
    fields = {}
    fields["B0"] = "[B0 -> D_s- K+]CC"
    fields["Ds"] = "[B0 -> ^D_s- K+]CC"
    fields["K"] = "[B0 -> D_s- ^K+]CC"

    spruce_line = "SpruceB2OC_BdToDsmK_DsmToHHH_FEST_Line"
    data_filtered = get_particles(f"/Event/Spruce/{spruce_line}/Particles")

    # DecayTreeFitter Algorithm.
    DTF = DecayTreeFitter(
        name="DTF_filtered",
        input_particles=data_filtered,
        mass_constraints=["D_s-"],
        output_level=INFO,
    )

    # make collection of functors for all particles
    variables_all = FunctorCollection(
        {
            "THOR_P": F.P,
            "THOR_PT": F.PT,
            "THOR_MASS": F.MASS,
        }
    )

    # make collection of functors for Ds meson
    variables_ds = FunctorCollection(
        {
            "DTF_PT": DTF(F.PT),
            "DTF_MASS": DTF(F.MASS),
            # Important note: specify an invalid value for integer functors if there exists no truth info.
            #                 The invalid value for floating point functors is set to nan.
            "DTF_CHILD1_ID": F.VALUE_OR(0) @ DTF(F.CHILD(1, F.PARTICLE_ID)),
            "DTF_CHILD1_MASS": DTF(F.CHILD(1, F.MASS)),
        }
    )

    # associate FunctorCollection to field (branch) name
    variables = {"ALL": variables_all, "Ds": variables_ds}

    filter_data = create_lines_filter("SpruceFilter", lines=[f"{spruce_line}"])

    # Configure Funtuple algorithm
    tuple_data = Funtuple(
        name="B0DsK_Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=data_filtered,
    )

    return make_config(options, [filter_data, tuple_data])
