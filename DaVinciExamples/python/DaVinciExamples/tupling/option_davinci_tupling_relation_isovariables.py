###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
r"""
Option file for testing the WeightedRelTableAlg algorithm.
The job runs over a Spruced sample and retrieves a set of :math:`B^0 \to K*(892)0 tau mu` candidates. For each candidate the algorithm
looks at the TES location which contains the extra particles to compute isolation and creates a 'one-to-many' relation map
relating all the available tracks to the signal candidates of the events (B and children).

Important: Setting DVPATH properly.
To run the example: $DVPATH/run lbexec option_davinci_tupling_relation_isovariables:main $DVPATH/DaVinciExamples/example_data/hlt2_b2ksttaumu_opt.yaml

rst_title: Relation Tables and Isolation Variables
rst_description: Example for testing the `WeightedRelTableAlg` algorithm and for applying ParticleIsolation and ConeIsolation functorcollection.
The job runs over a Spruced sample and retrieves a set of :math:`B^0 \to K*(892)0 tau mu` candidates.
For each candidate the algorithm looks at the TES location which contains the selection of extra particles and creates a 'one-to-many' relation map
relating all the available tracks to the isolated candidate of the events.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_relation_isovariables:main $DAVINCIEXAMPLESROOT/example_data/hlt2_b2ksttaumu_opt.yaml
rst_yaml: ../DaVinciExamples/example_data/hlt2_b2ksttaumu_opt.yaml
"""
from GaudiKernel.SystemOfUnits import GeV
from PyConf.Algorithms import WeightedRelTableAlg, ThOrParticleSelection
from PyConf.reading import get_particles, get_pvs
import Functors as F
from FunTuple import FunctorCollection, FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config


def main(options: Options):
    # Set branches
    branches = {
        "B": "[B0 -> ([J/psi(1S) -> tau+ mu-]CC)(K*(892)0 -> K+ pi-)]CC",
        "tau": "[B0 -> ([J/psi(1S) -> ^tau+ mu-]CC)(K*(892)0 -> K+ pi-)]CC",
        "mu": "[B0 -> ([J/psi(1S) -> tau+ ^mu-]CC)(K*(892)0 -> K+ pi-)]CC",
        "Kst": "[B0 -> ([J/psi(1S) -> tau+ mu-]CC)^(K*(892)0 -> K+ pi-)]CC",
    }

    FILTER_TREE = lambda id: F.FILTER(F.IS_ABS_ID(id)) @ F.GET_ALL_DESCENDANTS()

    b2ksttaumu_line = "Hlt2RD_BdToKstTauMu_KstToKPi_TauTo3Pi_OS"
    long_track_iso = "LongTrackIsolation"
    neutral_iso = "NeutralIsolation"

    # Get the selection of B and children candidates
    b2ksttaumu_data = get_particles(f"/Event/HLT2/{b2ksttaumu_line}/Particles")
    b2ksttaumu_kst_data = ThOrParticleSelection(
        InputParticles=b2ksttaumu_data, Functor=FILTER_TREE("K*(892)0")
    ).OutputSelection
    b2ksttaumu_tau_data = ThOrParticleSelection(
        InputParticles=b2ksttaumu_data, Functor=FILTER_TREE("tau+")
    ).OutputSelection
    b2ksttaumu_mu_data = ThOrParticleSelection(
        InputParticles=b2ksttaumu_data, Functor=FILTER_TREE("mu-")
    ).OutputSelection

    # Get the extra particles selected
    b_cciso_data = get_particles(
        f"/Event/HLT2/{b2ksttaumu_line}/B_{long_track_iso}/Particles"
    )
    kst_cciso_data = get_particles(
        f"/Event/HLT2/{b2ksttaumu_line}/Kst_{long_track_iso}/Particles"
    )
    tau_cciso_data = get_particles(
        f"/Event/HLT2/{b2ksttaumu_line}/tau_{long_track_iso}/Particles"
    )
    mu_cciso_data = get_particles(
        f"/Event/HLT2/{b2ksttaumu_line}/mu_{long_track_iso}/Particles"
    )
    b_nciso_data = get_particles(
        f"/Event/HLT2/{b2ksttaumu_line}/B_{neutral_iso}/Particles"
    )
    kst_nciso_data = get_particles(
        f"/Event/HLT2/{b2ksttaumu_line}/Kst_{neutral_iso}/Particles"
    )
    tau_nciso_data = get_particles(
        f"/Event/HLT2/{b2ksttaumu_line}/tau_{neutral_iso}/Particles"
    )
    mu_nciso_data = get_particles(
        f"/Event/HLT2/{b2ksttaumu_line}/mu_{neutral_iso}/Particles"
    )

    variables_all = FunctorCollection({"THOR_P": F.P, "THOR_PT": F.PT})

    # Add charge and neutral isolation variables with max dr=1 using ConeIsolation functorcollection
    b_cone_iso_variables = FC.ConeIsolation(
        name="1.0",
        head_cone=b2ksttaumu_data,
        charged_cone=b_cciso_data,
        neutral_cone=b_nciso_data,
        cut=(F.DR2 < 1.0),
    )
    tau_cone_iso_variables = FC.ConeIsolation(
        name="1.0",
        head_cone=b2ksttaumu_tau_data,
        charged_cone=tau_cciso_data,
        neutral_cone=tau_nciso_data,
        cut=(F.DR2 < 1.0),
    )
    mu_cone_iso_variables = FC.ConeIsolation(
        name="1.0",
        head_cone=b2ksttaumu_mu_data,
        charged_cone=mu_cciso_data,
        neutral_cone=mu_nciso_data,
        cut=(F.DR2 < 1.0),
    )
    kst_cone_iso_variables = FC.ConeIsolation(
        name="1.0",
        head_cone=b2ksttaumu_kst_data,
        charged_cone=kst_cciso_data,
        neutral_cone=kst_nciso_data,
        cut=(F.DR2 < 1.0),
    )

    pvs = get_pvs()

    # Build relation tables between head particles and extra particles using different cone size
    b_cc_isoAlg = WeightedRelTableAlg(
        ReferenceParticles=b2ksttaumu_data,
        InputCandidates=b_cciso_data,
        Cut=(F.DR2 < 0.5**2),
    )

    new_coneangle_str = "CC_0.5"
    # Add charge isolation variables with max dr=0.5 using ParticleIsolation functorcollection
    b_track_iso_variables = FC.ParticleIsolation(
        name=new_coneangle_str, iso_rel_table=b_cc_isoAlg
    )

    # Get the relation table from the output of the algorithm
    b_cc_isoAlg_Rels = b_cc_isoAlg.OutputRelations

    prefix = "HEAD_" + new_coneangle_str
    # Add extra variables to the branch of the B
    extra_variables = FunctorCollection(
        {
            f"{prefix}_Max_P": F.MAXCONE(Functor=F.P, Relations=b_cc_isoAlg_Rels),
            f"{prefix}_Min_P": F.MINCONE(Functor=F.P, Relations=b_cc_isoAlg_Rels),
            f"{prefix}_Max_PT": F.MAXCONE(Functor=F.PT, Relations=b_cc_isoAlg_Rels),
            f"{prefix}_Min_PT": F.MINCONE(Functor=F.PT, Relations=b_cc_isoAlg_Rels),
        }
    )

    # Change the selection requirement to make different relations
    b_cc_fixPV_isoAlg = WeightedRelTableAlg(
        ReferenceParticles=b2ksttaumu_data,
        InputCandidates=b_cciso_data,
        Cut=(F.SHARE_BPV(pvs) | (F.MINIPCHI2CUT(pvs, 9.0) @ F.FORWARDARG1())),
    )
    b_nc_massConstraints_isoAlg = WeightedRelTableAlg(
        ReferenceParticles=b2ksttaumu_data,
        InputCandidates=b_nciso_data,
        Cut=(F.COMB_MASS() < 8 * GeV),
    )

    # Add variables with different constraints
    b_fix_pv_iso_variables = FC.ParticleIsolation(
        name="CC_FixPV", iso_rel_table=b_cc_fixPV_isoAlg
    )
    b_mass_constraint_iso_variables = FC.ParticleIsolation(
        name="NC_MassConstraint", iso_rel_table=b_nc_massConstraints_isoAlg
    )

    # Add variables to the branches
    variables = {
        "B": variables_all
        + b_cone_iso_variables
        + b_track_iso_variables
        + extra_variables
        + b_fix_pv_iso_variables
        + b_mass_constraint_iso_variables,
        "tau": variables_all + tau_cone_iso_variables,
        "mu": variables_all + mu_cone_iso_variables,
        "Kst": variables_all + kst_cone_iso_variables,
    }

    my_filter = create_lines_filter(
        name="HDRFilter_BdToKstTauMu", lines=[f"{b2ksttaumu_line}"]
    )
    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=branches,
        variables=variables,
        inputs=b2ksttaumu_data,
    )
    return make_config(options, [my_filter, my_tuple])
