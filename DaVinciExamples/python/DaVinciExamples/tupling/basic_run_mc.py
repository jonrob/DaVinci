###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of a typical DaVinci job on MC

rst_title: Basic DaVinci Job on MC
rst_description: This example shows how to:

1. reconstruct and select two detached opposite-charge muons
2. add a tuple of the selected candidates.

rst_running: lbexec DaVinciExamples.tupling.basic_run_mc:main $DAVINCIEXAMPLESROOT/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml+$DAVINCIEXAMPLESROOT/example_options/example_tupling_basic_run_mc.yaml
rst_yaml: ../DaVinciExamples/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml+../DaVinciExamples/example_options/example_tupling_basic_run_mc.yaml
"""
import Functors as F
from Hlt2Conf.standard_particles import make_detached_mumu
from RecoConf.reconstruction_objects import upfront_reconstruction
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from DaVinci import Options, make_config


def main(options: Options):
    # Prepare the node with the selection
    dimuons = make_detached_mumu()

    # FunTuple: Jpsi info
    fields = {}
    fields["Jpsi"] = "J/psi(1S) -> mu+ mu-"
    fields["MuPlus"] = "J/psi(1S) -> ^mu+ mu-"

    # make collection of functors for Jpsi
    variables_jpsi = FunctorCollection(
        {
            "LOKI_P": "P",
            "LOKI_PT": "PT",
            "LOKI_Muonp_PT": "CHILD(PT, 1)",
            "LOKI_Muonm_PT": "CHILD(PT, 2)",
            "LOKI_MAXPT": "TRACK_MAX_PT",
            "LOKI_N_HIGHPT_TRCKS": "NINTREE(ISBASIC & HASTRACK & (PT > 1500*MeV))",
            "THOR_P": F.P,
            "THOR_PT": F.PT,
        }
    )

    # make collection of functors for Muplus
    variables_muplus = FunctorCollection({"LOKI_P": "P", "THOR_P": F.P})

    # associate FunctorCollection to (field branch) name
    variables = {}
    variables["Jpsi"] = variables_jpsi
    variables["MuPlus"] = variables_muplus

    # FunTuple: define list of preambles for loki
    loki_preamble = ["TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)"]

    # Configure Funtuple algorithm
    tuple_dimuons = Funtuple(
        name="DimuonsTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        loki_preamble=loki_preamble,
        inputs=dimuons,
    )

    return make_config(options, upfront_reconstruction() + [tuple_dimuons])
