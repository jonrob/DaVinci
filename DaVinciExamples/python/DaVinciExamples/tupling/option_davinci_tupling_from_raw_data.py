###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test for checking the correct processing of raw data.
rst_title: Tupling from raw data
rst_description: Test for checking the correct processing of raw data.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_from_raw_data:main $DAVINCIEXAMPLESROOT/example_data/Spruce_Run251342.yaml
rst_yaml: ../DaVinciExamples/example_data/Spruce_Run251342.yaml
"""
from PyConf.reading import get_particles, get_pvs
import Functors as F
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config

from PyConf.application import metainfo_repos

metainfo_repos.global_bind(extra_central_tags=["commissioning"])


def main(options: Options):
    #
    # Input line
    #
    line_KS2PiPi = "Hlt2Commissioning_KsToPimPip_LL"
    particles_KS2PiPi = get_particles(f"/Event/HLT2/{line_KS2PiPi}/Particles")

    fields_KS2PiPi = {
        "KS": "KS0 -> pi+ pi-",
        "pip": "KS0 -> ^pi+ pi-",
        "pim": "KS0 -> pi+ ^pi-",
    }

    v2_pvs = get_pvs()

    #
    # Variables for Ks.
    # BPVLTIME sometimes gets the right solution, sometimes nan and sometimes nonsense.
    # Reported as Rec#421.
    #
    variables = FunctorCollection(
        {
            "BPVFDCHI2": F.BPVFDCHI2(v2_pvs),
            "BPVFD": F.BPVFD(v2_pvs),
            "BPVLTIME": F.BPVLTIME(v2_pvs),
            "BPVIP": F.BPVIP(v2_pvs),
            "BPVIPCHI2": F.BPVIPCHI2(v2_pvs),
            "CHI2": F.CHI2,
            "END_VX": F.END_VX,
            "END_VY": F.END_VY,
            "END_VZ": F.END_VZ,
            "BPVX": F.BPVX(v2_pvs),
            "BPVY": F.BPVY(v2_pvs),
            "BPVZ": F.BPVZ(v2_pvs),
        }
    )

    # FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = FC.Kinematics()

    #
    # Variables for pions. Just a technical test of functors.
    # PID is uncalibrated and many PID variables return 0.
    # PROBNN_D and PROBNN_MU presently returns nan.
    #
    all_vars = {}
    all_vars["PID_E"] = F.PID_E
    all_vars["PID_K"] = F.PID_K
    all_vars["PID_MU"] = F.PID_MU
    all_vars["PID_P"] = F.PID_P
    all_vars["PID_PI"] = F.PID_PI
    # POD
    all_vars["PROBNN_D"] = F.PROBNN_D
    all_vars["PROBNN_E"] = F.PROBNN_E
    all_vars["PROBNN_GHOST"] = F.PROBNN_GHOST
    all_vars["PROBNN_K"] = F.PROBNN_K
    all_vars["PROBNN_MU"] = F.PROBNN_MU
    all_vars["PROBNN_P"] = F.PROBNN_P
    all_vars["PROBNN_PI"] = F.PROBNN_PI

    variables_extra = FunctorCollection(all_vars)
    # FunTuple: associate functor collections to field (branch) name
    variables_KS2PiPi = {
        "ALL": variables_all,  # adds variables to all fields
        "KS": variables,
        "pip": variables_extra,
        "pim": variables_extra,
    }

    #
    # Event variables
    #
    evt_vars = FC.EventInfo()
    evt_vars["PV_SIZE"] = F.SIZE(v2_pvs)

    tuple_KS2PiPi = Funtuple(
        name="Tuple_KS2PiPi",
        tuple_name="DecayTree",
        fields=fields_KS2PiPi,
        variables=variables_KS2PiPi,
        event_variables=evt_vars,
        inputs=particles_KS2PiPi,
    )

    filter_KS2PiPi = create_lines_filter(
        name="HDRFilter_KS2PiPi", lines=[f"{line_KS2PiPi}"]
    )

    algs = {"KS2PiPi": [filter_KS2PiPi, tuple_KS2PiPi]}

    return make_config(options, algs)
