###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test for checking the correct processing of Spruce data.
rst_title: Tupling from Spruce data
rst_description: Test for checking the correct processing of Spruce data.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_from_spruce:main $DAVINCIEXAMPLESROOT/example_data/Spruce_all_lines_dst.yaml+$DAVINCIEXAMPLESROOT/example_options/example_tupling_from_spruce.yaml
rst_yaml: ../DaVinciExamples/example_data/Spruce_all_lines_dst.yaml+../DaVinciExamples/example_options/example_tupling_from_spruce.yaml
"""
from PyConf.reading import get_particles
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config


def main(options: Options):
    line_B0DsK = "SpruceB2OC_BdToDsmK_DsmToHHH_FEST"
    line_B0Dspi = "SpruceB2OC_BdToDsmPi_DsmToKpKmPim"
    bd2dsk_line = get_particles(f"/Event/Spruce/{line_B0DsK}/Particles")
    bd2dspi_line = get_particles(f"/Event/Spruce/{line_B0Dspi}/Particles")

    fields_dsk = {
        "B0": "[B0 -> D_s- K+]CC",
        "Ds": "[B0 -> ^D_s- K+]CC",
        "Kp": "[B0 -> D_s- ^K+]CC",
    }

    fields_dspi = {
        "B0": "[B0 -> D_s- pi+]CC",
        "Ds": "[B0 -> ^D_s- pi+]CC",
        "pip": "[B0 -> D_s- ^pi+]CC",
    }

    variables = FunctorCollection(
        {
            "LOKI_MAXPT": "TRACK_MAX_PT",
            "LOKI_Muonp_PT": "CHILD(PT, 1)",
            "LOKI_Muonm_PT": "CHILD(PT, 2)",
        }
    )

    variables_extra = FunctorCollection(
        {"LOKI_NTRCKS_ABV_THRSHLD": "NINTREE(ISBASIC & (PT > 15*MeV))"}
    )
    variables += variables_extra

    # FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = FC.Kinematics()

    # FunTuple: associate functor collections to field (branch) name
    variables_dsk = {
        "ALL": variables_all,  # adds variables to all fields
        "B0": variables,
        "Ds": variables_extra,
        "Kp": variables_extra,
    }

    variables_dspi = {
        "ALL": variables_all,  # adds variables to all fields
        "B0": variables,
        "Ds": variables_extra,
        "pip": variables_extra,
    }

    loki_preamble = ["TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)"]

    tuple_B0DsK = Funtuple(
        name="B0DsK_Tuple",
        tuple_name="DecayTree",
        fields=fields_dsk,
        variables=variables_dsk,
        loki_preamble=loki_preamble,
        inputs=bd2dsk_line,
    )

    tuple_B0Dspi = Funtuple(
        name="B0Dspi_Tuple",
        tuple_name="DecayTree",
        fields=fields_dspi,
        variables=variables_dspi,
        loki_preamble=loki_preamble,
        inputs=bd2dspi_line,
    )

    filter_B0DsK = create_lines_filter(name="HDRFilter_B0DsK", lines=[f"{line_B0DsK}"])
    filter_B0Dspi = create_lines_filter(
        name="HDRFilter_B0Dspi", lines=[f"{line_B0Dspi}"]
    )

    algs = {
        "B0DsK": [filter_B0DsK, tuple_B0DsK],
        "B0Dspi": [filter_B0Dspi, tuple_B0Dspi],
    }

    return make_config(options, algs)
