###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test for checking the correct processing of Spruce MC.
rst_title: Tupling from Spruce MC
rst_description: Test for checking the correct processing of Spruce MC.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_from_spruce_mc:main $DAVINCIEXAMPLESROOT/example_data/test_spruce_MCtools.yaml
rst_yaml: ../DaVinciExamples/example_data/test_spruce_MCtools.yaml
"""
from FunTuple import FunctorCollection
import Functors as F
from FunTuple import FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC
from DaVinci import Options, make_config
from DaVinciMCTools import MCTruthAndBkgCat
from DaVinci.algorithms import create_lines_filter
from PyConf.reading import get_particles


def main(options: Options):
    # FunTuple: define branches.
    fields = {
        "B0": "[B0 -> D_s- K+]CC",
        "Ds": "[B0 -> ^D_s- K+]CC",
        "Kp": "[B0 -> D_s- ^K+]CC",
    }

    # FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = FC.Kinematics()

    # FunTuple: associate functor collections to branch name
    variables = {
        "ALL": variables_all,  # adds variables to all branches
    }

    line_name = "Spruce_Test_line"
    B_data = get_particles(f"/Event/Spruce/{line_name}/Particles")

    my_filter = create_lines_filter(name="HDRFilter_B", lines=[f"{line_name}"])

    # get configured "MCTruthAndBkgCatAlg" algorithm for HLT2 output
    MCTRUTH = MCTruthAndBkgCat(B_data, name="MCTruthAndBkgCat_spruce")
    # Add trueid info to each of the branches
    trueid_bkgcat_info = {
        # Important note: specify an invalid value for integer functors if there exists no truth info.
        #                 The invalid value for floating point functors is set to nan.
        "TRUEID": F.VALUE_OR(0) @ MCTRUTH(F.PARTICLE_ID),
        "TRUEKEY": F.VALUE_OR(-1) @ MCTRUTH(F.OBJECT_KEY),
        "TRUEPT": MCTRUTH(F.PT),
        "TRUEPX": MCTRUTH(F.PX),
        "TRUEPY": MCTRUTH(F.PY),
        "TRUEPZ": MCTRUTH(F.PZ),
        "TRUEENERGY": MCTRUTH(F.ENERGY),
        "TRUEP": MCTRUTH(F.P),
        "TRUEFOURMOMENTUM": MCTRUTH(F.FOURMOMENTUM),
        "BKGCAT": MCTRUTH.BkgCat,
    }
    for branch in variables.keys():
        variables[branch] += FunctorCollection(trueid_bkgcat_info)

    my_tuple = Funtuple(
        name="Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=B_data,
    )

    return make_config(options, [my_filter, my_tuple])
