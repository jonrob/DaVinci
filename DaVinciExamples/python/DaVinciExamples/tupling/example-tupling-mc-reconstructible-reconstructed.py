###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example options to show the usage of the new DaVinciMCTools
`MCReconstructible` and `MCReconstructed` helper classes.

rst_title: Reconstructible and reconstructed candidates with DaVinciMCTools
rst_description: This example shows how to:

1. get variables related to reconstructible and reconstructed information on candidates
2. create a tuple of the candidates matching a specific decay chain

rst_running: lbexec DaVinciExamples.tupling.example-tupling-mc-reconstructible-reconstructed:main $DAVINCIEXAMPLESROOT/example_data/test_hlt1_trigger_decisions.yaml
rst_yaml: ../DaVinciExamples/example_data/test_hlt1_trigger_decisions.yaml
"""
from PyConf.reading import get_mc_particles, get_pp2mcp_relations, get_mc_track_info
from FunTuple import FunTuple_MCParticles as MCFuntuple
from DaVinciMCTools import MCReconstructible as MCRectible, MCReconstructed as MCRected
from DaVinci import Options, make_config
import FunTuple.functorcollections as FC


def main(options: Options):
    # Input
    MC_data = get_mc_particles("/Event/HLT2/MC/Particles")

    # PP2MCP relations
    relations_charged = get_pp2mcp_relations("/Event/HLT2/Relations/ChargedPP2MCP")
    relations_neutral = get_pp2mcp_relations("/Event/HLT2/Relations/NeutralPP2MCP")

    # Get variables related to reconstructible information.
    mcrtible = MCRectible(
        input_mctrackinfo=get_mc_track_info()
    )  # tes location of MC track info
    # The option extra_info is set to False by default and can be set to True to get more information
    vars_rtible = FC.MCReconstructible(mcreconstructible_alg=mcrtible, extra_info=True)
    print("Reconstructible functors:", vars_rtible.functor_dict.keys())
    # Note instead of importing functorcollections (FC.MCReconstructible), one
    # can directly add track related information using the helper class (DaVinciMCTools.MCReconstructible)
    vars_rtible["EXTRA_MC_HASUT"] = mcrtible.HasUT
    vars_rtible["EXTRA_MC_HASVELO"] = mcrtible.HasVelo
    print("Reconstructible functors:", vars_rtible.functor_dict.keys())

    # Get variables related to reconstructed information.
    # - If "use_best_mcmatch = True" (default), the best associated reconstructed
    #   track to the mc particle is used (tupling scalars).
    # - If "use_best_mcmatch = False", all associated reconstructed
    #   tracks to the mc particle are used (tupling arrays).
    #   Here we set it to false for testing purposes.
    mcrted_all = MCRected(
        input_mcparticles=MC_data,
        use_best_mcmatch=False,
        relations_charged=relations_charged,
        relations_neutral=relations_neutral,
    )
    # The option extra_info below is set to False by default in the functor collection.
    vars_rted = FC.MCReconstructed(mcreconstructed_alg=mcrted_all, extra_info=True)
    # Note:
    # - Instead of importing functorcollections (FC.MCReconstructed), one
    #   can directly add track related information using the helper class (DaVinciMCTools.MCReconstructed).
    # - A new functor can be added for e.g. vars_rtible["MC_HASUT"] =  mcrtible(F.NEW_TRACK_FUNCTOR)
    vars_rted["EXTRA_TRACK_TYPE[TRACK_INDX]"] = mcrted_all.TrackType
    vars_rted["EXTRA_TRACK_HASUT[TRACK_INDX]"] = mcrted_all.HasUT
    print("Reconstructed functors:", vars_rted.functor_dict.keys())

    # Variables
    variables = {
        "ALL": vars_rted,
        # Apply reconstructible to charged particles
        "Kplus": vars_rtible,
        "pip": vars_rtible,
        "pim1": vars_rtible,
        "pim2": vars_rtible,
    }

    # Define fields
    fields = {
        "B0": "[[B0]CC -> (D- -> K+ pi- pi-) pi+]CC",
        "Dm": "[[B0]CC -> ^(D- -> K+ pi- pi-) pi+]CC",
        "Kplus": "[[B0]CC -> (D- -> ^K+ pi- pi-) pi+]CC",
        "pip": "[[B0]CC -> (D- -> K+ pi- pi-) ^pi+]CC",
        "pim1": "[[B0]CC -> (D- -> K+ ^pi- pi-) pi+]CC",
        "pim2": "[[B0]CC -> (D- -> K+ pi- ^pi-) pi+]CC",
    }

    # Make tuple algorithm
    tuple_Dpi = MCFuntuple(
        name="DpiMC",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=MC_data,
    )
    # Run
    return make_config(options, [tuple_Dpi])
