###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of how to run DecayTreeFitter in DaVinci

rst_title: DecayTreeFitter in DaVinci
rst_description: This example shows how to:

1. select two detached opposite-charge muons
2. create a tuple of the selected candidates
3. run DecayTreeFitterAlg and stores some output

rst_running: lbexec DaVinciExamples.tupling.DTF_run_mc:main $DAVINCIEXAMPLESROOT/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml+$DAVINCIEXAMPLESROOT/example_options/example_tupling_DTF_run_mc.yaml
rst_yaml: ../DaVinciExamples/example_data/Spruce_all_lines_dst.yaml+../DaVinciExamples/example_options/example_tupling_DTF_run_mc.yaml
"""
import Functors as F
from Hlt2Conf.standard_particles import make_detached_mumu
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.reconstruction_objects import make_pvs_v1
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from DecayTreeFitter import DecayTreeFitter
from DaVinci import Options, make_config


def main(options: Options):
    # Prepare the node with the selection
    dimuons = make_detached_mumu()
    pvs = make_pvs_v1()

    # DecayTreeFitter Algorithm.
    # One with PV constraint and one without

    DTF = DecayTreeFitter(
        name="DTF_dimuons",
        input_particles=dimuons,
        mass_constraints=["J/psi(1S)"],
        output_level=3,
    )

    # FunTuple: Jpsi info
    fields = {}
    fields["Jpsi"] = "J/psi(1S) -> mu+ mu-"
    fields["MuPlus"] = "J/psi(1S) -> ^mu+ mu-"

    # make collection of functors for Jpsi
    variables_jpsi = FunctorCollection(
        {
            "LOKI_P": "P",
            "LOKI_PT": "PT",
            "LOKI_Muonp_PT": "CHILD(PT, 1)",
            "LOKI_Muonm_PT": "CHILD(PT, 2)",
            "LOKI_MAXPT": "TRACK_MAX_PT",
            "LOKI_N_HIGHPT_TRCKS": "NINTREE(ISBASIC & HASTRACK & (PT > 1500*MeV))",
            "THOR_P": F.P,
            "THOR_PT": F.PT,
            "THOR_MASS": F.MASS,
            "DTF_PT": DTF(F.PT),
            "DTF_MASS": DTF(F.MASS),
        }
    )

    #
    # Another way of adding variables.
    #
    DTF_pv = DecayTreeFitter(
        name="DTF_PVConstraints",
        input_particles=dimuons,
        input_pvs=pvs,  # input PVs
        mass_constraints=["J/psi(1S)"],
    )
    variables_jpsi.update(
        DTF_pv.apply_functors(functors=[F.PT, F.MASS], head="DTF_PV_")
    )

    # make collection of functors for Muplus
    variables_muplus = FunctorCollection(
        {
            "LOKI_P": "P",
            "THOR_P": F.P,
            "DTF_PT": DTF(F.PT),
        }
    )

    # associate FunctorCollection to field (branch) name
    variables = {}
    variables["Jpsi"] = variables_jpsi
    variables["MuPlus"] = variables_muplus

    # FunTuple: define list of preambles for loki
    loki_preamble = ["TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)"]

    # Configure Funtuple algorithm
    tuple_dimuons = Funtuple(
        name="DimuonsTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        loki_preamble=loki_preamble,
        inputs=dimuons,
    )

    return make_config(options, upfront_reconstruction() + [tuple_dimuons])
