###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
r"""
Example of an advanced DaVinci job

rst_title: Advanced DaVinci Job on MC
rst_description: This example shows how to:

1. reconstruct and select two detached opposite-charge muons
2. add a tuple of the selected :math:`J/\psi` candidates.
3. reconstruct and select two detached opposite-charge pions
4. add a tuple of the selected :math:`K^0_S` candidates.

rst_running: lbexec DaVinciExamples.tupling.advanced_run_mc:main $DAVINCIEXAMPLESROOT/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml+$DAVINCIEXAMPLESROOT/example_options/example_tupling_advanced_run_mc.yaml
rst_yaml: ../DaVinciExamples/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml+../DaVinciExamples/example_options/example_tupling_advanced_run_mc.yaml
"""
import Functors as F
from Hlt2Conf.standard_particles import make_detached_mumu, make_KsDD
from RecoConf.reconstruction_objects import upfront_reconstruction
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
import FunTuple.functorcollections as FC
from DaVinci import Options, make_config


def main(options: Options):
    # Prepare the node with the selection
    dimuons = make_detached_mumu()
    kshorts = make_KsDD()
    """
    Tuple observables related to Jpsi -> mu+ mu-
    """
    # FunTuple: make fields (branches) to tuple (NB: adding a special keyword 'ALL' with throw an error)
    fields = {}
    fields["Jpsi"] = "J/psi(1S) -> mu+ mu-"
    fields["MuPlus"] = "J/psi(1S) -> ^mu+ mu-"

    # FunTuple: make collection of functors for Jpsi
    variables_jpsi = FunctorCollection(
        {
            "PT": F.PT,
            "PX": F.PX,
            "PY": F.PY,
            "PZ": F.PZ,
            "ENERGY": F.ENERGY,
            "FourMom_P": F.FOURMOMENTUM,
            "MAXPT": "TRACK_MAX_PT",
        }
    )
    # FunTuple: extend the collection given a dictionary
    # In case of common entries (i.e. common key irrespective of functor code) the program warns user and overwrites the previous entry
    variables_jpsi.update(
        {"PT": F.PT}
    )  # OR like dictionaries can do variables_jpsi['LOKI_PT'] = 'PT'
    # FunTuple: Remove from collection given the list of variable names
    variables_jpsi.pop(["PT", "ENERGY"])
    # FunTuple: Join two functor
    # If entries already exist user is warned about picking entries from the base instance.
    variables_extra = FunctorCollection({"ENERGY": F.ENERGY, "P": F.P})
    variables_jpsi += variables_extra
    # FunTuple: Can also subtract two FunctorCollection to get unique entries like below
    # variables_unique = variables_jpsi - variables_extra
    print("Variables in Jpsi field", variables_jpsi)

    # FunTuple: make functor collection from the imported functor library Kinematics
    variables_all = FC.Kinematics()
    print("Variables in ALL field", variables_all)
    print("Variables in MuPlus field", variables_extra)

    # FunTuple: associate functor collections to field (branch) name
    variables = {}
    variables["ALL"] = variables_all  # adds variables to all fields
    variables["Jpsi"] = variables_jpsi
    variables["MuPlus"] = variables_extra

    # FunTuple: define list of preambles for loki
    loki_preamble = ["TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)"]

    # FunTuple: Configure Funtuple algorithm
    tuple_dimuons = Funtuple(
        name="DimuonsTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        loki_preamble=loki_preamble,
        inputs=dimuons,
    )
    """
    Tuple observables related to KS0 -> 2pi
    """
    # FunTuple: As above make fields (branches)
    fields_KS = {}
    fields_KS["KS"] = "KS0 -> pi+ pi-"
    # FunTuple: Associate the functor collections to field names
    variables_KS = {}
    variables_KS["ALL"] = variables_all
    variables_KS["KS"] = variables_jpsi
    # FunTuple: Configure Funtuple algorithm
    tuple_kshorts = Funtuple(
        name="KsTuple",
        tuple_name="DecayTree",
        fields=fields_KS,
        variables=variables_KS,
        loki_preamble=loki_preamble,
        inputs=kshorts,
    )

    algs = {
        "Reco": upfront_reconstruction(),
        "DiMuons": [tuple_dimuons],
        "KShorts": [tuple_kshorts],
    }

    # Algorithms will be run following the insertion order.
    # In this case, both "DiMuons" and "KShorts" need locations created by
    # upfront_reconstruction() as input, so a "Reco" node is created on top.
    from PyConf.control_flow import CompositeNode, NodeLogic

    node_dimuons = CompositeNode(
        "DiMuons", children=algs["DiMuons"], combine_logic=NodeLogic.LAZY_AND
    )
    node_kshorts = CompositeNode(
        "KShorts", children=algs["KShorts"], combine_logic=NodeLogic.LAZY_AND
    )
    node_tuples = CompositeNode(
        "Tupling",
        children=[node_dimuons, node_kshorts],
        combine_logic=NodeLogic.NONLAZY_AND,
    )
    node = CompositeNode(
        "Reco", children=algs["Reco"] + [node_tuples], combine_logic=NodeLogic.LAZY_AND
    )

    return make_config(options, [node])
