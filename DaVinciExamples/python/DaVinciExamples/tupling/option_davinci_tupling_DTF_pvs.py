###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
r"""option_davinci_tupling_DTF_pvs.py
Example options to show the usage of the DecayTreeFitter with PV constraints.
rst_title: DecayTreeFitter with PV constraints
rst_description: This example shows how to use DecayTreeFitter with PV constraints.
It runs two different versions of the DecayTreeFitter algorithm: the first version uses the best PV constraint and v1 PV, while the second version considers all possible PV constraints and uses v2 PVs.
rst_running: lbexec DaVinciExamples.tupling.option_davinci_tupling_DTF_pvs:main $DAVINCIEXAMPLESROOT/example_data/test_passthrough_thor_lines.yaml
rst_yaml: ../DaVinciExamples/example_data/test_passthrough_thor_lines.yaml
"""

from PyConf.reading import get_particles, get_pvs, get_pvs_v1
from DecayTreeFitter import DecayTreeFitter
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import functorcollections as FC
from DaVinci.algorithms import create_lines_filter
from DaVinci import Options, make_config


def main(options: Options):
    B_Line = "Hlt2B2CC_BsToJpsiPhi_Detached"
    B_Data = get_particles(f"/Event/HLT2/{B_Line}/Particles")

    my_filter = create_lines_filter(name="HDRFilter_Bs2JpsiPhi", lines=[B_Line])

    # DTF works for both v1 and v2 vertices
    pvs_v1 = get_pvs_v1()
    pvs_v2 = get_pvs()

    fields = {
        "Bs": "[ B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K-) ]CC",
        "Jpsi": "[ B_s0 -> ^(J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K-) ]CC",
        "Phi": "[ B_s0 -> (J/psi(1S) -> mu+ mu-) ^(phi(1020) -> K+ K-) ]CC",
        "MuP": "[ B_s0 -> (J/psi(1S) -> ^mu+ mu-) (phi(1020) -> K+ K-) ]CC",
        "MuM": "[ B_s0 -> (J/psi(1S) -> mu+ ^mu-) (phi(1020) -> K+ K-) ]CC",
        "KP": "[ B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> ^K+ K-) ]CC",
        "KM": "[ B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ ^K-) ]CC",
    }

    DTF_BestPV = DecayTreeFitter(
        name="DTF_BestPV",
        input_particles=B_Data,
        mass_constraints=["B_s0", "J/psi(1S)"],
        input_pvs=pvs_v1,
        fit_all_pvs=False,
    )

    DTF_AllPVs = DecayTreeFitter(
        name="DTF_AllPVs",
        input_particles=B_Data,
        mass_constraints=["B_s0", "J/psi(1S)"],
        input_pvs=pvs_v2,
        fit_all_pvs=True,
    )

    variables = {
        "Bs": FC.DecayTreeFitterResults(
            DTF=DTF_BestPV,
            prefix="DTF_BestPV",
            decay_origin=True,
            with_lifetime=True,
            with_kinematics=False,
        )
        + FC.DecayTreeFitterResults(
            DTF=DTF_AllPVs,
            prefix="DTF_AllPVs",
            decay_origin=True,
            with_lifetime=True,
            with_kinematics=False,
        ),
        "ALL": FC.DecayTreeFitterResults(
            DTF=DTF_BestPV,
            prefix="DTF_BestPV",
            decay_origin=False,
            with_lifetime=False,
            with_kinematics=True,
        )
        + FC.DecayTreeFitterResults(
            DTF=DTF_AllPVs,
            prefix="DTF_AllPVs",
            decay_origin=False,
            with_lifetime=False,
            with_kinematics=True,
        ),
    }

    # Configure Funtuple algorithm
    funtuple = Funtuple(
        name="JpsiPhi_Tuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        inputs=B_Data,
    )

    # Run
    algs = {
        "JpsiPhi_Tuple": [my_filter, funtuple],
    }
    return make_config(options, algs)
