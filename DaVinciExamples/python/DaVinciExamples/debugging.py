###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of a DaVinci job printing decay trees via `PrintDecayTree`.

rst_title: Printing Decay Trees
rst_description: This example shows how to print the decay tree of a candidate.
rst_running: lbexec DaVinciExamples.debugging:print_decay_tree "$DAVINCIEXAMPLESROOT/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml"
rst_yaml: ../DaVinciExamples/example_data/Upgrade_Bd2KstarMuMu_ldst.yaml
"""
from PyConf.application import configure, configure_input
from PyConf.application import make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import PrintDecayTree, PrintHeader

from RecoConf.reconstruction_objects import upfront_reconstruction

from DaVinci import Options
from DaVinci.common_particles import make_std_loose_jpsi2mumu


def print_decay_tree(options: Options):
    jpsis = make_std_loose_jpsi2mumu()

    pdt = PrintDecayTree(name="PrintJpsis", Input=jpsis)

    # the "upfront_reconstruction" is what unpacks reconstruction objects, particles and primary vertices
    # from file and creates protoparticles.
    algs = upfront_reconstruction() + [jpsis, pdt]

    node = CompositeNode(
        "PrintJpsiNode", children=algs, combine_logic=NodeLogic.NONLAZY_AND
    )

    config = configure_input(options)
    config.update(configure(options, node))
    return config


def print_header(options: Options):
    ph = PrintHeader(name="PrintHeader", ODINLocation=make_odin())

    node = CompositeNode("PHNode", children=[ph])

    config = configure_input(options)
    config.update(configure(options, node))
    return config
