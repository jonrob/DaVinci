<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <argument name="args"><set>
    <text>DaVinciExamples.tupling.option_davinci_tupling_tistos:main</text>
  </set></argument>
  <argument name="options_yaml_fn"><text>$DAVINCIEXAMPLESROOT/example_data/test_tistos.yaml</text></argument>
  <argument name="extra_options_yaml"><text>
    histo_file: tistos_histos.root
    ntuple_file: tistos_ntuple.root
    print_freq: 1
  </text></argument>
  <argument name="reference"><text>../refs/test_davinci_tupling_tistos.ref</text></argument>
  <argument name="error_reference"><text>../refs/empty.ref</text></argument>
  <argument name="validator"><text>
from DaVinciTests.QMTest.DaVinciExclusions import preprocessor, counter_preprocessor
##validator complains about long TTreeNames so comment out(see https://gitlab.cern.ch/lhcb/DaVinci/-/issues/46)
#validateWithReference(preproc = preprocessor, counter_preproc = counter_preprocessor)

import sys, os, glob
from ROOT import TFile

B_vars_stored =['phi2_Hlt1TwoTrackMVADecision_TIS', 'Km2_Hlt1TwoTrackMVADecision_TOS', 'Km2_PT', 'Kp2_PT', 'Bs_Hlt1TrackMVADecision_TOS', 'Bs_PT', 'Kp2_Hlt1TwoTrackMVADecision_TIS', 'Km1_Hlt1TwoTrackMVADecision_TIS', 'phi1_Hlt1TwoTrackMVADecision_TOS', 'phi1_Hlt1TwoTrackMVADecision_TIS', 'Kp2_Hlt1TrackMVADecision_TOS', 'Kp2_Hlt1TrackMVADecision_TIS', 'Kp2_Hlt1TwoTrackMVADecision_TOS', 'Bs_Hlt1TrackMVADecision_TIS', 'phi1_Hlt1TrackMVADecision_TOS', 'phi2_Hlt1TrackMVADecision_TOS', 'Kp1_Hlt1TwoTrackMVADecision_TOS', 'Km2_Hlt1TwoTrackMVADecision_TIS', 'Kp1_Hlt1TrackMVADecision_TOS', 'Km1_Hlt1TrackMVADecision_TIS', 'Km1_Hlt1TwoTrackMVADecision_TOS', 'phi2_PT', 'Kp1_Hlt1TwoTrackMVADecision_TIS', 'phi1_Hlt1TrackMVADecision_TIS', 'phi2_Hlt1TrackMVADecision_TIS', 'phi1_PT', 'Hlt1TrackMVADecision', 'Hlt1_TCK', 'Kp1_Hlt1TrackMVADecision_TIS', 'Hlt1TwoTrackMVADecision', 'Kp1_PT', 'Km2_Hlt1TrackMVADecision_TIS', 'Bs_Hlt1TwoTrackMVADecision_TOS', 'Km1_Hlt1TrackMVADecision_TOS', 'Km2_Hlt1TrackMVADecision_TOS', 'Km1_PT', 'Bs_Hlt1TwoTrackMVADecision_TIS', 'phi2_Hlt1TwoTrackMVADecision_TOS']
B_vars_stored += ['RUNNUMBER', 'EVENTNUMBER']

#sort the expected vars
B_vars_stored = sorted(B_vars_stored)

#open the TFile and TTree
ntuple = './tistos_ntuple.root'
if not os.path.isfile(ntuple): raise Exception(f"File: {ntuple} does not exist!")
f      = TFile.Open(ntuple)
t_B    = f.Get('Tuple/DecayTree')

#sort the stores vars
b_names = sorted([b.GetName() for b in t_B.GetListOfLeaves()])

B_excluded_1 = set(B_vars_stored) - set(b_names)
B_excluded_2 = set(b_names) - set(B_vars_stored)
if len(B_excluded_1) != 0: raise Exception('Number of stored variables is less than what is expected. The extra variables expected are: ' , B_excluded_1)
if len(B_excluded_2) != 0: raise Exception('Number of stored variables is greater than what is expected. The extra variables stored are: ', B_excluded_2)

entry_num = 0
for entry in t_B:
    tos = entry.Bs_Hlt1TrackMVADecision_TOS
    tis = entry.Bs_Hlt1TrackMVADecision_TIS
    dec = entry.Hlt1TrackMVADecision
    #None of the candidates we ran are TOB i.e. (!tis and !tos and dec)
    # we check that (tis | tos) == dec
    tos_or_tis = tos or tis
    if tos_or_tis != dec:
        raise Exception(f'The entry # {entry_num} is perhaps TOB? i.e. (tis | tos) is {tos_or_tis} and decision is {dec}. However we did not run over a sample with TOB candidates. Please check!')
    entry_num += 1

f.Close()

countErrorLines({"FATAL":0, "ERROR":0})
  </text></argument>
</extension>
