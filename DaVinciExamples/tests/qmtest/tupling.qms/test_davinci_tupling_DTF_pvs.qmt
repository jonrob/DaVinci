<?xml version="1.0" ?>
<!--
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>lbexec</text></argument>
  <argument name="args"><set>
    <text>DaVinciExamples.tupling.option_davinci_tupling_DTF_pvs:main</text>
  </set></argument>
  <argument name="options_yaml_fn"><text>$DAVINCIEXAMPLESROOT/example_data/test_passthrough_thor_lines.yaml</text></argument>
  <argument name="extra_options_yaml"><text>
    print_freq: 100
    histo_file: dtf_pvs_histo.root
    ntuple_file: dtf_pvs_tuple.root
    evt_max: 100
  </text></argument>
  <argument name="validator"><text>
from DaVinciTests.QMTest.DaVinciExclusions import remove_known_warnings
from PyConf.components import findRootObjByDir
countErrorLines({"FATAL": 0, "ERROR": 0},
                stdout=remove_known_warnings(stdout))
import sys, os
from ROOT import TFile


B_vars_stored = ['Bs_DTF_AllPVs_CHI2', 'Bs_DTF_AllPVs_CHI2DOF', 'Bs_DTF_AllPVs_FD', 'Bs_DTF_AllPVs_FDERR', 'Bs_DTF_AllPVs_IDX', 'Bs_DTF_AllPVs_MASS', 'Bs_DTF_AllPVs_MASSERR', 'Bs_DTF_AllPVs_NDOF', 'Bs_DTF_AllPVs_NITER', 'Bs_DTF_AllPVs_P', 'Bs_DTF_AllPVs_PE', 'Bs_DTF_AllPVs_PERR', 'Bs_DTF_AllPVs_PV_KEY', 'Bs_DTF_AllPVs_PV_X', 'Bs_DTF_AllPVs_PV_Y', 'Bs_DTF_AllPVs_PV_Z', 'Bs_DTF_AllPVs_PX', 'Bs_DTF_AllPVs_PY', 'Bs_DTF_AllPVs_PZ', 'Bs_DTF_AllPVs_TAU', 'Bs_DTF_AllPVs_TAUERR', 'Bs_DTF_BestPV_CHI2', 'Bs_DTF_BestPV_CHI2DOF', 'Bs_DTF_BestPV_FD', 'Bs_DTF_BestPV_FDERR', 'Bs_DTF_BestPV_MASS', 'Bs_DTF_BestPV_MASSERR', 'Bs_DTF_BestPV_NDOF', 'Bs_DTF_BestPV_NITER', 'Bs_DTF_BestPV_P', 'Bs_DTF_BestPV_PE', 'Bs_DTF_BestPV_PERR', 'Bs_DTF_BestPV_PV_KEY', 'Bs_DTF_BestPV_PV_X', 'Bs_DTF_BestPV_PV_Y', 'Bs_DTF_BestPV_PV_Z', 'Bs_DTF_BestPV_PX', 'Bs_DTF_BestPV_PY', 'Bs_DTF_BestPV_PZ', 'Bs_DTF_BestPV_TAU', 'Bs_DTF_BestPV_TAUERR', 'DTF_AllPVs_IDX', 'EVENTNUMBER', 'Jpsi_DTF_AllPVs_IDX', 'Jpsi_DTF_AllPVs_PE', 'Jpsi_DTF_AllPVs_PX', 'Jpsi_DTF_AllPVs_PY', 'Jpsi_DTF_AllPVs_PZ', 'Jpsi_DTF_BestPV_PE', 'Jpsi_DTF_BestPV_PX', 'Jpsi_DTF_BestPV_PY', 'Jpsi_DTF_BestPV_PZ', 'KM_DTF_AllPVs_IDX', 'KM_DTF_AllPVs_PE', 'KM_DTF_AllPVs_PX', 'KM_DTF_AllPVs_PY', 'KM_DTF_AllPVs_PZ', 'KM_DTF_BestPV_PE', 'KM_DTF_BestPV_PX', 'KM_DTF_BestPV_PY', 'KM_DTF_BestPV_PZ', 'KP_DTF_AllPVs_IDX', 'KP_DTF_AllPVs_PE', 'KP_DTF_AllPVs_PX', 'KP_DTF_AllPVs_PY', 'KP_DTF_AllPVs_PZ', 'KP_DTF_BestPV_PE', 'KP_DTF_BestPV_PX', 'KP_DTF_BestPV_PY', 'KP_DTF_BestPV_PZ', 'MuM_DTF_AllPVs_IDX', 'MuM_DTF_AllPVs_PE', 'MuM_DTF_AllPVs_PX', 'MuM_DTF_AllPVs_PY', 'MuM_DTF_AllPVs_PZ', 'MuM_DTF_BestPV_PE', 'MuM_DTF_BestPV_PX', 'MuM_DTF_BestPV_PY', 'MuM_DTF_BestPV_PZ', 'MuP_DTF_AllPVs_IDX', 'MuP_DTF_AllPVs_PE', 'MuP_DTF_AllPVs_PX', 'MuP_DTF_AllPVs_PY', 'MuP_DTF_AllPVs_PZ', 'MuP_DTF_BestPV_PE', 'MuP_DTF_BestPV_PX', 'MuP_DTF_BestPV_PY', 'MuP_DTF_BestPV_PZ', 'Phi_DTF_AllPVs_IDX', 'Phi_DTF_AllPVs_PE', 'Phi_DTF_AllPVs_PX', 'Phi_DTF_AllPVs_PY', 'Phi_DTF_AllPVs_PZ', 'Phi_DTF_BestPV_PE', 'Phi_DTF_BestPV_PX', 'Phi_DTF_BestPV_PY', 'Phi_DTF_BestPV_PZ', 'RUNNUMBER']

# The hash is got from DaVinci/v63r6 with 5 significant digit
#                      dd4hep platform       desc platform
expected_chi2_hashs = [-6712918245859272476, -8420881755086790226, -7952819373100832545]

#sort the expected vars
B_vars_stored = sorted(B_vars_stored)

#open the TFile and TTree
ntuple = 'dtf_pvs_tuple.root'
if not os.path.isfile(ntuple): raise Exception(f"File: {ntuple} does not exist!")
f      = TFile.Open(ntuple)
t_B    = findRootObjByDir(f, 'JpsiPhi_Tuple', 'DecayTree')

#sort the stores vars
b_names = sorted([b.GetName() for b in t_B.GetListOfLeaves()])

B_excluded_1 = set(B_vars_stored) - set(b_names)
B_excluded_2 = set(b_names) - set(B_vars_stored)
if len(B_excluded_1) != 0: raise Exception('Number of stored variables is less than what is expected. The extra variables expected are: ' , B_excluded_1)
if len(B_excluded_2) != 0: raise Exception('Number of stored variables is greater than what is expected. The extra variables stored are: ', B_excluded_2)

from DaVinciTests.QMTest.check_helpers import get_hash_from_branch
got_chi2_hash = get_hash_from_branch(ntuple, "JpsiPhi_Tuple/DecayTree", 'Bs_DTF_BestPV_CHI2', significant_digits=5)
if not got_chi2_hash in expected_chi2_hashs:
  msg_str = f"Output tuple has unexpected values in JpsiPhi_Tuple/DecayTree/Bs_DTF_BestPV_CHI2, got_hash = {got_chi2_hash}, expected_hashs = {expected_chi2_hashs}"
  causes.append(msg_str)

f.Close()
os.system(f"rm {ntuple}")

  </text></argument>
</extension>
