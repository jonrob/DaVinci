# General information

The project contains several active and maintained branches, such as the `201X-patches` branches.
They are all protected, meaning that code cannot be pushed into them directly but only through merge requests (MRs).
This helps with the validation of code prior to making it available in the official branches for future releases.

## Where to commit code to

- Changes and bug fixes for Run 1 and Run 2 analysis (or re-reconstruction, re-stripping) should go to the `run2-patches` branch.
  They will then be propagated to `master` (if relevant also for Upgrade) by the applications managers.

- Any changes specific to Upgrade should *only* got to `master`.

- For details of which branches support the different versions of Run 1 and Run 2 strippings on both real data and simulation, see https://gitlab.cern.ch/lhcb/Analysis/-/blob/master/CONTRIBUTING.md

-  Things may be different for bug fixes to old and/or specific versions of DaVinci, in which case it is probably best to discuss
unless you know exactly what you are doing.

In doubt, please get in touch before creating a MR.
