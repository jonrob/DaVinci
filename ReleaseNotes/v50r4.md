2019-05-22 DaVinci v50r4
========================

Development release prepared on the master branch.

It is based on Gaudi v32r0, LHCb v50r4, Lbcom v30r4, Rec v30r4, Phys v30r4, Analysis v30r4 and Stripping v11r10.
It uses LCG_95 with ROOT 6.16.00.

- Bring DaVinciTests/tests/qmtest/commonparticles.qms tests in sync
  - See merge request lhcb/DaVinci!319
- Remove obsolete file
  - See merge request lhcb/DaVinci!318
- Clean-up of ever-lasting failing test
  - See merge request lhcb/DaVinci!316
- Remove ever-lasting FSR-related tests
  - See merge request lhcb/DaVinci!317
- Update of a couple of CommonParticles tests
  - See merge request lhcb/DaVinci!312
- Update of tests to follow printout changes upstream
  - See merge request lhcb/DaVinci!311
