2023-05-16 DaVinci v63r8
===

This version uses
Analysis [v41r8](../../../../Analysis/-/tags/v41r8),
Moore [v54r8p1](../../../../Moore/-/tags/v54r8p1),
Allen [v3r8](../../../../Allen/-/tags/v3r8),
Rec [v35r8](../../../../Rec/-/tags/v35r8),
Lbcom [v34r8](../../../../Lbcom/-/tags/v34r8),
LHCb [v54r8](../../../../LHCb/-/tags/v54r8),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12),
Detector [v1r12](../../../../Detector/-/tags/v1r12) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to DaVinci [v63r7](/../../tags/v63r7), with the following changes:

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Update References for: Rec!3396, Analysis!976, DaVinci!894 based on lhcb-master-mr/7845, !905 (@lhcbsoft)
- Update References for: LHCb!4106, Alignment!376, Analysis!981, DaVinci!899 based on lhcb-master-mr/7800, !900 (@lhcbsoft)
- Follow changes in LHCb!4106, !899 (@graven)
- Follow changes in LHCb!4110, !898 (@graven)
- Update References for: LHCb!4055, Moore!2227, DaVinci!889 based on lhcb-master-mr/7770, !896 (@lhcbsoft)
- Follow-up from https://gitlab.cern.ch/lhcb/Analysis/-/merge_requests/977, !895 (@amathad)
- Follow-up from Rec!3396, !894 (@amathad)
- Followup from Rec!3375 (improve invalid value handling for tistos), !893 (@amathad)
- ~Functors | Add example and functors related to matching particles by VELO LHCbIDs, !890 (@ldufour)
- Follow changes in LHCb!4055, !889 (@graven)
- Upstream project highlights :star:

### Documentation ~Documentation

- Fix the docs pipeline pinning urllib3 1.x, !903 (@erodrigu)
- Adding a comment about qmtexec to the documentation, !892 (@msaur)
