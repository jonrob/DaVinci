2023-01-27 DaVinci v63r4
===

This version uses
Analysis [v41r4](../../../../Analysis/-/tags/v41r4),
Moore [v54r4](../../../../Moore/-/tags/v54r4),
Rec [v35r4](../../../../Rec/-/tags/v35r4),
Allen [v3r4](../../../../Allen/-/tags/v3r4),
Lbcom [v34r4](../../../../Lbcom/-/tags/v34r4),
LHCb [v54r4](../../../../LHCb/-/tags/v54r4),
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9),
Detector [v1r8](../../../../Detector/-/tags/v1r8) and
LCG [101a](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on the `master` branch.
Built relative to DaVinci [v63r3](/../../tags/v63r3), with the following changes:

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- New test on raw data, !818 (@pkoppenb) [#80]
- Add a protection against Geant info in stderr, !812 (@pkoppenb)
- Upstream project highlights :star:


### Documentation ~Documentation

- Fix DV version in tutorial documentation, !819 (@amathad) [#93]


### Other

- ~Functors | Making DaVinci MC tools more consistent (adapt to https://gitlab.cern.ch/lhcb/Analysis/-/merge_requests/945), !813 (@erodrigu)
- ~Functors ~Tuples | Tests for new functor to access Hlt1 TIS/TOS information, !771 (@oozcelik)
- ~Tuples | Tuple run and event number by default (Follow up from  https://gitlab.cern.ch/lhcb/Analysis/-/merge_requests/929), !784 (@amathad)
- Fixed ntuple file name in test_davinci_tupling_tistos, !811 (@sponce)
- Use env variable to address issue https://gitlab.cern.ch/lhcb/DaVinci/-/issues/87, !798 (@pkoppenb)
